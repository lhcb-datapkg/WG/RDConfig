# RDConfig - Important Information


Before adding your own filtering scripts or using old filtering scripts please check the full documentation here:
https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/FilteredSimulationProduction#How_to_make_a_stripping_filtered

Some things worked in the past and don't work with up-to-date stripping versions.
Be especially aware that for Stripping21(rXpY) [2011+2012], 24r2[2015] or 28r2 [2016] productions, you must add these lines to your basic options:
```python
#Items that get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs
```

