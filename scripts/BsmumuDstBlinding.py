#######################################################################
#
#  Code to produce Blind DSTs from stripping 19a data with DV v30r4
# 
########################################################################
    
from Gaudi.Configuration import *
from Configurables import FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, MultiSelectionSequence
from Configurables import ( DaVinci, MessageSvc, FilterDesktop )

#
# Create selection sequences
#


#
# BhhBlind selection: all Bs2MuMu/B2hh candidates except signal region (BLIND)
#

MyBsmumu = DataOnDemand(Location='/Event/Dimuon/Phys/Bs2MuMuLinesNoMuIDLine/Particles')

BhhBlind = FilterDesktop('BhhBlind')

#Set mass cut on 20120531 from prelim fit by elsass
BhhBlind.Code = (
    "((2 > NINTREE((ABSID=='mu+') & (ISMUON) ))  "\
    "| ((2 == NINTREE((ABSID=='mu+') & (ISMUON) )) "\
    " & ((MM<5223*MeV) | (MM>5429*MeV)))) "\
    " & (2 == NINTREE((ABSID=='mu+') & (P<500*GeV) & (PT<40*GeV)))"\
    " & (INTREE( (ABSID=='B_s0') &(PT>200*MeV) & (BPVLTIME()<13.248*ps) )) "
    )


SelBhhBlind = Selection('SelBhhBlind',
                    Algorithm = BhhBlind,
                    RequiredSelections = [MyBsmumu])
SeqBhhBlind = SelectionSequence('SeqBhhBlind', TopSelection = SelBhhBlind)


MyBsmumuwide = DataOnDemand(Location='/Event/Dimuon/Phys/Bs2MuMuLinesWideMassLine/Particles')
SelBsmumuWideBlind = Selection('SelBsmumuWideBlind',
                    Algorithm = BhhBlind,
                    RequiredSelections = [MyBsmumuwide])
SeqBsmumuWideBlind = SelectionSequence('SeqBsmumuWideBlind', TopSelection = SelBsmumuWideBlind)

MyBsmumuloose = DataOnDemand(Location='/Event/Dimuon/Phys/Bs2MuMuLinesNoMuIDLooseLine/Particles')
SelBsmumuLooseBlind = Selection('SelBsmumuLooseBlind',
                    Algorithm = BhhBlind,
                    RequiredSelections = [MyBsmumuloose])
SeqBsmumuLooseBlind = SelectionSequence('SeqBsmumuLooseBlind', TopSelection = SelBsmumuLooseBlind)


#
# Control sample candidate already made in stripping19
#



MyBu = DataOnDemand(Location='/Event/Dimuon/Phys/Bs2MuMuLinesBu2JPsiKLine/Particles')

BuTight = FilterDesktop('BuTight')
BuTight.Code = (
    "   (INTREE( (ABSID=='B+') & (BPVDIRA>0.)  ))"\
    "& (2 == NINTREE((ABSID=='mu+') & (ISMUON) ))"\
    "& (1 == NINTREE((ABSID=='K+')  & (ISLONG) ))"
    )


    
SelBuTight = Selection('SelBu2JPsiK',
                    Algorithm = BuTight,
                    RequiredSelections = [MyBu])
SeqBuTight = SelectionSequence('SeqBuTight', TopSelection = SelBuTight)


MyBd = DataOnDemand(Location='/Event/Dimuon/Phys/Bs2MuMuLinesBd2JPsiKstLine/Particles')
BdTight = FilterDesktop('BdTight')

BdTight.Code = (
    "   (INTREE( (ABSID=='B0') & (BPVDIRA>0.)  ))"\
    "& (2 == NINTREE((ABSID=='mu+') & (ISMUON) ))"\
    "& (1 == NINTREE((ABSID=='K+')  & (ISLONG) ))"\
    "& (1 == NINTREE((ABSID=='pi+') & (ISLONG) ))"
    )


SelBdTight = Selection('SelBd2JPsiKstar',
                    Algorithm = BdTight,
                    RequiredSelections = [MyBd])
SeqBdTight = SelectionSequence('SeqBdTight', TopSelection = SelBdTight)

MyBs = DataOnDemand(Location='/Event/Dimuon/Phys/Bs2MuMuLinesBs2JPsiPhiLine/Particles')
BsTight = FilterDesktop('BsTight')
BsTight.Code = (
    "(INTREE( (ABSID=='B_s0') & (BPVDIRA>0.)))"\
    "& (2 == NINTREE((ABSID=='mu+') & (ISMUON) ))"\
    "& (2 == NINTREE((ABSID=='K+')  & (ISLONG) ))"
    )

SelBsTight = Selection('SelBs2JPsiPhi',
                    Algorithm = BsTight,
                    RequiredSelections = [MyBs])
SeqBsTight = SelectionSequence('SeqBsTight', TopSelection = SelBsTight)



#
# BhhUnBlind selection: all Bs2MuMu/B2hh candidates with tight cuts
#

MyBsmumu = DataOnDemand(Location='/Event/Dimuon/Phys/Bs2MuMuLinesNoMuIDLine/Particles')

BhhUnblind = FilterDesktop('BhhUnblind')
#SB.Code = "ALL"
BhhUnblind.Code = (
    "  (2 == NINTREE((ABSID=='mu+') & (P<500*GeV) & (PT<40*GeV)))"\
    " & (INTREE( (ABSID=='B_s0') &(PT>200*MeV) & (BPVLTIME()<13.248*ps) )) "
    )

SelBhhUnblind = Selection('SelBhhUnblind',
                    Algorithm = BhhUnblind,
                    RequiredSelections = [MyBsmumu])
SeqBhhUnblind = SelectionSequence('SeqBhhUnblind', TopSelection = SelBhhUnblind)


MyBsmumuwide = DataOnDemand(Location='/Event/Dimuon/Phys/Bs2MuMuLinesWideMassLine/Particles')
SelBsmumuWideUnblind = Selection('SelBsmumuWideUnblind',
                    Algorithm = BhhUnblind,
                    RequiredSelections = [MyBsmumuwide])
SeqBsmumuWideUnblind = SelectionSequence('SeqBsmumuWideUnblind', TopSelection = SelBsmumuWideUnblind)

MyBsmumuloose = DataOnDemand(Location='/Event/Dimuon/Phys/Bs2MuMuLinesNoMuIDLooseLine/Particles')
SelBsmumuLooseUnblind = Selection('SelBsmumuLooseUnblind',
                    Algorithm = BhhUnblind,
                    RequiredSelections = [MyBsmumuloose])
SeqBsmumuLooseUnblind = SelectionSequence('SeqBsmumuLooseUnblind', TopSelection = SelBsmumuLooseUnblind)



#-----------------------
# Write  DSTs
#----------------------

multiSeqBlind = MultiSelectionSequence('BsmumuBlind', Sequences = [SeqBhhBlind, SeqBsmumuWideBlind,SeqBsmumuLooseBlind, SeqBuTight, SeqBdTight, SeqBsTight])
multiSeqUnblind = MultiSelectionSequence('BsmumuUnblind', Sequences = [SeqBhhUnblind, SeqBsmumuWideUnblind,SeqBsmumuLooseUnblind, SeqBuTight, SeqBdTight, SeqBsTight])



from Configurables import SelDSTWriter, DaVinci

# modifiaction to have only one DSTWriter as it is what is expected for WGProd#
# Its name shoud be "MyDSTWriter".
dstWriter = SelDSTWriter( "MyDSTWriter",
                          OutputFileSuffix ='000000',
                          SelectionSequences = [multiSeqBlind,multiSeqUnblind]
                          )


#
# Configure DaVinci
#
       
from Configurables import DaVinci
DaVinci().EvtMax = -1                         # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = "2012"                    #
DaVinci().Simulation   = False
#DaVinci().HistogramFile = "DVHistos.root"     # Histogram file
#DaVinci().TupleFile = "Tuple.root"   
DaVinci().UserAlgorithms = [ dstWriter.sequence() ]
DaVinci().Lumi = True
#EventSelector().Input   = ["/var/pcjm/r02/lhcb/mbettler/00018407_00001006_1.dimuon.dst"]
#DaVinci().RedoMCLinks  = True
#DaVinci().EnableUnpack = True
#DaVinci().appendToMainSequence( [PrintDecayTree(InputLocations = ['/Event/Dimuon/Phys/Bs2MuMuBs2MuMuLinesNoMuIDLine']) ] )



#from Configurables import CondDB
#CondDB(UseOracle = True)
#stripping 17
#DDDBtag = "head-20110914"
#CondDBtag = "head-20110914"
