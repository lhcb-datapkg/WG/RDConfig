"""
Options for building Stripping20r0p1,
on Bu2Kpipigamma phsp MC.

@author: Giovanni Veneziano (giovanni.veneziano@cern.ch)

"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Build the streams and stripping object
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20r0p1'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

_radiative = quickBuild('Radiative')
_radiative.lines[:] = [ x for x in _radiative.lines if 'B2K1Gamma' in x.name() ]
for line in _radiative.lines :
   print "Matching radiative line called " + line.name()

streams.append( _radiative )

# turn off all pre-scalings
for stream in streams:
    for line in stream.lines:
        line._prescale = 1.0

# Merge into one stream and run in flag mode
AllStreams = StrippingStream("Bu2Kpipigamma.Strip")

for stream in streams:
    AllStreams.appendLines(stream.lines)

sc = StrippingConf(Streams=[AllStreams],
                   MaxCandidates=2000,
                   TESPrefix='Strip')

AllStreams.sequence().IgnoreFilterPassed = False # if True, we get all events written out


#Now apply selection
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand, MergedSelection
from Configurables          import FilterDesktop
from Configurables          import CombineParticles, CheckPV
from Configurables import LoKi__Hybrid__TupleTool

_strippedEvent = DataOnDemand(Location="/Event/Phys/B2K1Gamma_B2VG_Line/Particles")
preambulo = ["goodTrack = (HASTRACK) & (PT > 500.0*MeV)",
             "goodPhoton = (ABSID=='gamma') & (PT > 3000.*MeV) & (CL > 0.25)",
             "goodK1 = (ABSID=='K_1(1270)+') & (MIPCHI2DV(PRIMARY) > 0.)",
             "goodB = (ABSID=='B+') & (PT > 2500.*MeV) "]
code = "(NINTREE(goodTrack) == 3) & (NINTREE(goodPhoton) == 1) & (NINTREE(goodK1) == 1) & (goodB)"
sel = FilterDesktop("CandFilter", Code=code, Preambulo=preambulo)

MyBpSel = Selection("MyBpSel", Algorithm=sel, RequiredSelections=[_strippedEvent])
MyBpSequence = SelectionSequence("MyBpSequence", TopSelection=MyBpSel)

# Configuration of SelDSTWriter
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements

SelDSTWriterElements = {'default' : stripDSTElements(pack=enablePacking)}

SelDSTWriterConf = {'default': stripDSTStreamConf(pack=enablePacking)}


dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='Filtered',
                         SelectionSequences=[MyBpSequence])

# Add stripping TCK
from Configurables import StrippingTCK
#stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x32252001)
stck = StrippingTCK(HDRLocation='/Event/Strip/Phys/DecReports',
                    TCK=0x409f0045) # this is the one in the BKK

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([stck])
DaVinci().appendToMainSequence([MyBpSequence.sequence()])
DaVinci().appendToMainSequence([dstWriter.sequence()])
DaVinci().ProductionType = "Stripping"

#options
DaVinci().Simulation = True
DaVinci().InputType = 'DST'
DaVinci().DataType = "2012"
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60

from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
DaVinci().MoniSequence += [DumpFSR()]

# EOF

