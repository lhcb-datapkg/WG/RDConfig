from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand
from Configurables import FilterDesktop
from Configurables import LoKiSvc
from Configurables import FilterInTrees

# from CommonParticlesArchive import CommonParticlesArchiveConf
# CommonParticlesArchiveConf().redirect('stripping28')


MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci


_stdPions = DataOnDemand(Location = "Phys/StdLoosePions/Particles")
_pionFilter = FilterDesktop('pionFilter', 
                    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
                    Code = "(PT > 300 *MeV) & (P> 2000 *MeV) & (TRCHI2DOF < 5.0) & (MIPCHI2DV(PRIMARY) > 5.0)")#" & (mcMatch('[pi+]cc'))" )

_PionFilterSel = Selection(name = 'PionFilterSel',
                          Algorithm = _pionFilter,
                          RequiredSelections = [ _stdPions ])


_stdMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
_muonFilter = FilterDesktop('muonFilter',
               Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
               Code = "(PT> 300 *MeV) & (P> 2000 *MeV) & (TRCHI2DOF < 5.0) & (MIPCHI2DV(PRIMARY) > 5.0)")

_MuonFilterSel = Selection(name = 'MuonFilterSel',
                          Algorithm = _muonFilter,
                          RequiredSelections = [ _stdMuons ])

############################################################################
#RECOMBINE THE LEPTONS TO A PHI


_makephi = CombineParticles("phi",
                            DecayDescriptor = "phi(1020) -> mu+ mu-",
                            #InputLocations  = [ 'StdAllLooseElectrons' ],
                            Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
                            # DaughtersCuts= {
                            #                 'e+':"(mcMatch(' phi(1020) -> e+ e-'))",
                            #                 'e-':"(mcMatch(' phi(1020) -> e+ e-'))",
                            #                 }
                          )

_makephi.MotherCut = "(BPVDIRA>0.9999) & (VFASPF(VCHI2/VDOF) < 5.0)" # 'phi': "" in order to cut uninteresting events from the data BPV = Best Primary Vertex
#_makephi.CombinationCut = "(ACHILD(PT,1)+ACHILD(PT,2) > 400 *MeV)"

_phi_sel = Selection(name="phi_sel",
                    Algorithm=_makephi,
                    RequiredSelections=[_MuonFilterSel])

                          
############
#Remake the Ds
_makeD = CombineParticles("D+",
                            Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
                            # DaughtersCuts= {
                            #                  'pi+':"(mcMatch(' D+ -> phi(1020) pi+'))",
                            #                  'pi-':"(mcMatch(' D- -> phi(1020) pi-'))",
                            #                 #'phi(1020)':"(mcMatch(' (Charm & Meson) -> ^phi(1020) pi+ pi-'))",
                            #                 }
                            )

_makeD.DecayDescriptor = "[D+ -> phi(1020) pi+]cc"
_makeD.CombinationCut = " (ADAMASS('D+') < 800 *MeV) "\
                        " & (AMAXDOCA('') < 0.15*mm)"\
                         "& (ACHILD(M,1) > 250 *MeV)"
                        #" & (ACHILD(PT,1)+ACHILD(PT,2) > 800.*MeV)"\
                        #" & (ACHILD(MIPCHI2DV(PRIMARY),1)+ACHILD(MIPCHI2DV(PRIMARY),2)> 5.0) "


_makeD.MotherCut =  "(ADMASS('D+') < 800 *MeV)"\
                    "& (VFASPF(VCHI2/VDOF) < 5.0) " \
                    "& (BPVDIRA> 0.9999)" \
                    "& (BPVVDCHI2 > 25.0) " \
                    #"& (MIPCHI2DV(PRIMARY) > 9.0)"

_D_sel = Selection(name="D_sel",
                    Algorithm=_makeD,
                    RequiredSelections=[_phi_sel, _PionFilterSel]) #the order matters!! It makes sense to put the rarer decay first in order to 
                                                                            #save time -> put the most distinctive decay first

D_phi_pi_sel_seq = SelectionSequence("Dphipimm.SafeStrip", 
                          TopSelection = _D_sel)

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          #OutputFileSuffix ='Filtered',
                          SelectionSequences = [D_phi_pi_sel_seq]
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1 # Number of events
DaVinci().appendToMainSequence( [ D_phi_pi_sel_seq.sequence(), dstWriter.sequence()  ] )


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60