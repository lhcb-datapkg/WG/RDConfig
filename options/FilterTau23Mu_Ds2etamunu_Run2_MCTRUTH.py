from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 5 ],
                    "CloneDistCut" : [5000, 9e+99 ] }


from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive


from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand
from Configurables import FilterDesktop
from Configurables import LoKiSvc

from StandardParticles import StdLooseAllPhotons


# truth matching standard particle


_stdMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
_muonFilter = FilterDesktop('muonFilter',
                            Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
                            Code = " (mcMatch('[mu+]cc'))")

MuonFilterSel = Selection(name = 'MuonFilterSel',
                          Algorithm = _muonFilter,
                          RequiredSelections = [ _stdMuons ])


# truth matching eta                                                                                                                                                      


_makeEta = CombineParticles("make_eta",
                            DecayDescriptor = "[eta -> mu- mu+ gamma]cc",
                            CombinationCut =  "(ADAMASS('eta') < 500 *MeV)",
                            MotherCut = "mcMatch( '[ eta => mu- mu+ gamma ]CC' ) | mcMatch( '[ eta=> mu+ mu- gamma ]CC' )" )

selEta = Selection("SelEta",
                   Algorithm = _makeEta,
                   RequiredSelections = [ MuonFilterSel, StdLooseAllPhotons] )


# truth matching Ds


_makeDs = CombineParticles("make_Ds",
                           DecayDescriptor = "[D_s+ -> eta mu+ ]cc",
                           MotherCut = "mcMatch( '[ D_s+ => eta mu+ nu_mu ]CC' ) | mcMatch( '[ D_s- => eta mu- nu_mu~ ]CC' )" )

selDs = Selection("SelDs",
                  Algorithm = _makeDs,
                  RequiredSelections = [ MuonFilterSel, selEta ] )
seq = SelectionSequence("MCTrue", TopSelection=selDs )

   


#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                  stripDSTStreamConf,
                                  stripDSTElements
                                  )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
         }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
         }

dstWriter = SelDSTWriter( "MyDSTWriter",
                                                 StreamConf = SelDSTWriterConf,
                                                 MicroDSTElements = SelDSTWriterElements,
                                                 OutputFileSuffix ='Filtered_tau23mu_Ds2etamunu_Run2',
                                                 SelectionSequences = [seq]
                                                 )

from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ seq.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

# TO DELETE
