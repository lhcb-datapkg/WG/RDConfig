"""
Stripping34r0p1 filtered production for all radiative streams. Selects any stripping
lines whose name includes either "Beauty2X" or "Lb", in addition to "Gamma".

Outputs to DST.

Retention rate of 8% when tested on Bd2KstGamma (11102202) events.

Adapted from FilterBeauty2XGamma-S34r0p1-dst.py.

@author Izaac Sanderswood
@date   2021-11-05
"""
'''
#------Remove---------
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#---------------------
'''

#stripping version
stripping='stripping34r0p1'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = []
streams.append( buildStream(stripping=config, streamName='Leptonic', archive=archive) )
streams.append( buildStream(stripping=config, streamName='BhadronCompleteEvent', archive=archive) )

AllStreams = StrippingStream("AllRadiative.Strip")

linesToAdd = []
for stream in streams:
    for line in stream.lines:
        if ( 'Beauty2X' in line.name() or 'Lb' in line.name() ) and 'Gamma' in line.name():
        		line._prescale = 1.0
        		linesToAdd.append(line)
        		 
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip' )


AllStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,
                                                 #selectiveRawEvent=True,
                                                 fileExtension='.dst')
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44103401)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"

#------Remove---------
#DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
#---------------------
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

'''
#######################################################################
# comment this part out
# tested with ` lb-run DaVinci/v44r11p1 gaudirun.py FilterAllRadiative-S34r0p1-dst.py `
DaVinci().DataType = "2018"
# Save info of rejection factor in the filtering
from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output2018.txt"
DaVinci().MoniSequence += [ DumpFSR() ]

from GaudiConf import IOHelper
# Use the local input data

# S34 Bd->K*gamma
#evt+std://MC/2018/11102202/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/ALLSTREAMS.DST
IOHelper().inputFiles([
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00086521/0000/00086521_00000002_7.AllStreams.dst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00086521/0000/00086521_00000003_7.AllStreams.dst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00086521/0000/00086521_00000004_7.AllStreams.dst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00086521/0000/00086521_00000005_7.AllStreams.dst'
    ], clear=True)
'''

