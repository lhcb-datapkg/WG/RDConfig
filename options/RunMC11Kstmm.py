from Configurables import DaVinci
DaVinci().EvtMax = 100
#MC options
DaVinci().DDDBtag = "MC11-20111020"
DaVinci().CondDBtag = "MC11-20111020-vc-md100"
DaVinci().MainOptions  = ""
from Configurables import EventSelector
EventSelector().Input =  [  'root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/MC/MC11a/ALLSTREAMS.DST/00013335/0000/00013335_00000002_1.allstreams.dst?svcClass=lhcbdisk']
EventSelector().PrintFreq = 10
from Configurables import LoKiSvc
LoKiSvc().Welcome = False

