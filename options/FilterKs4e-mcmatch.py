"""
MCMatch Filtering file for Ks->eeee
@author Adrian Casais Vidal, Xabier Cid Vidal, Carla Marin
@date 2018-07-26
"""

from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from Configurables import ChargedProtoParticleMaker,ChargedPP2MC,NoPIDsParticleMaker,EventSelector
from PhysSelPython.Wrappers import Selection, SelectionSequence

from StandardParticles import StdAllNoPIDsElectrons, StdNoPIDsUpElectrons

from CommonParticles.Utils import *

#
## Making VELO tracks available
#


myprotos = ChargedProtoParticleMaker("MyProtoParticles",
                                     Inputs = ["Rec/Track/Best"],
                                     Output = "Rec/ProtoP/MyProtoParticles")

protop_locations = [myprotos.Output]
charged = ChargedPP2MC("myprotos")
charged.InputData = protop_locations
myseq = GaudiSequencer("myseq")
myseq.Members +=[myprotos,charged]


                
algorithm =  NoPIDsParticleMaker ( 'StdNoPIDsVeloElectronsLocal',
                                   DecayDescriptor = 'Electron' ,
                                   Particle = 'electron',
                                   AddBremPhotonTo= [],
                                   Input = myprotos.Output)



selector = trackSelector ( algorithm,trackTypes = [ "Velo" ]  )
locations = updateDoD ( algorithm )

from PhysSelPython.Wrappers import AutomaticData

StdNoPIDsVeloElectrons = AutomaticData(Location = "Phys/StdNoPIDsVeloElectronsLocal/Particles")
                                      

_sels = {"Long":StdAllNoPIDsElectrons,"Velo":StdNoPIDsVeloElectrons,"Up":StdNoPIDsUpElectrons}
MatchedElectrons={}
MatchedElectrons_seq={}
locations = {}

#
## Matched electrons locations
#

for key in ["Long","Velo","Up"]:
    if key == "Velo":
	    _loc = "'/Event/Relations/Rec/ProtoP/MyProtoParticles'"
	    _code = "( mcMatch('KS0 => ^e+ e- e+ e-', %s )) | (mcMatch('KS0 => e+ ^e- e+ e-', %s )) | (mcMatch('KS0 => e+ e- ^e+ e-', %s )) | (mcMatch('KS0 => e+ e- e+ ^e-', %s ))"%(_loc,_loc,_loc,_loc)
    if key == "Up":
	    _loc = "'Relations/Rec/ProtoP/Charged'"
	    _code = "( mcMatch('KS0 => ^e+ e- e+ e-', %s )) | (mcMatch('KS0 => e+ ^e- e+ e-', %s )) | (mcMatch('KS0 => e+ e- ^e+ e-', %s )) | (mcMatch('KS0 => e+ e- e+ ^e-', %s ))"%(_loc,_loc,_loc,_loc)
    if key == "Long":
	    _code = "( mcMatch('KS0 => ^e+ e- e+ e-')) | (mcMatch('KS0 => e+ ^e- e+ e-')) | (mcMatch('KS0 => e+ e- ^e+ e-')) | (mcMatch('KS0 => e+ e- e+ ^e-'))"
    
    preambulo =[ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ]
    _Filter = FilterDesktop(Preambulo = preambulo,Code = _code)
    
    MatchedElectrons[key]  = Selection("Matched"+key+"Electrons",Algorithm = _Filter, RequiredSelections = [_sels[key]]   )
				       
    MatchedElectrons_seq[key] = SelectionSequence("Matched"+key+"ElectronsSequence", TopSelection=MatchedElectrons[key])
     

#
## Setting up combinations
#

combs = {"LL":"( (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 4 ))",
         "UU":"( (ANUM(  ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 2)  & (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 2) )",
         "VV":"( (ANUM(  ( TRTYPE == 1 ) &  ( ABSID == 'e-' ) ) == 2)  & (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 2) ) ",
         "LU":"( (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 3)  & (ANUM(  ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 1) )",
         "LV":"( (ANUM(  ( TRTYPE == 1 ) &  ( ABSID == 'e-' ) ) == 1)  & (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 3) )",
         "UV":"( (ANUM(  ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 1)  & (ANUM(  ( TRTYPE == 1 ) &  ( ABSID == 'e-' ) ) == 1) & (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 2)  )"}



selsKs4e={}
seqsks2eeee={}
Ks2eeee={}
for name in combs:
    Ks2eeee[name] = CombineParticles("TrackSel"+name+"_Ks2eeee")

    if "V" in name:
        Ks2eeee[name].DecayDescriptors = ["KS0 -> e+ e- e+ e-","KS0 -> e+ e- e+ e+","KS0 -> e+ e- e- e-"]
	
    else:
        Ks2eeee[name].DecayDescriptor = "KS0 -> e+ e- e+ e-"
    Ks2eeee[name].Preambulo=["from LoKiPhysMC.decorators import *",
                               "from LoKiPhysMC.functions import mcMatch"]
     

    
    Ks2eeee[name].CombinationCut = combs[name]
    Ks2eeee[name].MotherCut = "ALL"
    


    Ks2eeee[name].Inputs =['Phys/MatchedLongElectrons/Particles']
    if "U" in name:
	    Ks2eeee[name].Inputs+=['Phys/MatchedUpElectrons/Particles']
    if "V" in name:
	    Ks2eeee[name].Inputs+=['Phys/MatchedVeloElectrons/Particles']
    

    if name=="UV":
	    selsKs4e[name]= Selection("SelKs2eeee_"+name,
			      Algorithm = Ks2eeee[name],
                              
			      RequiredSelections=[AutomaticData(Location = 'Phys/MatchedLongElectrons/Particles'),
    			       			  AutomaticData(Location ='Phys/MatchedUpElectrons/Particles'),
    			       			  AutomaticData(Location = 'Phys/MatchedVeloElectrons/Particles')])
	    
    elif "U" in name:
	    selsKs4e[name]= Selection("SelKs2eeee_"+name,
			      Algorithm = Ks2eeee[name],
                              
			      RequiredSelections=[AutomaticData(Location = 'Phys/MatchedLongElectrons/Particles'),
    			       			  AutomaticData(Location ='Phys/MatchedUpElectrons/Particles')
    			       			  ])
    elif "V" in name:
	    selsKs4e[name]= Selection("SelKs2eeee_"+name,
			      Algorithm = Ks2eeee[name],
                              
			      RequiredSelections=[AutomaticData(Location = 'Phys/MatchedLongElectrons/Particles'),
    			       			  AutomaticData(Location ='Phys/MatchedVeloElectrons/Particles')
    			       			  ])
    else: 
	    selsKs4e[name]= Selection("SelKs2eeee_"+name,
			      Algorithm = Ks2eeee[name],
                              
			      RequiredSelections=[AutomaticData(Location = 'Phys/MatchedLongElectrons/Particles')
    			       			  ])
				    
    
                       

#
## Merging streams
#

from StrippingConf.Configuration import StrippingConf
from StrippingConf.StrippingLine import StrippingLine
from StrippingConf.StrippingStream import StrippingStream

strilines = {}
for key in selsKs4e:
	strilines[key] = StrippingLine("line"+key,
				       prescale  = 1,
				       postscale = 1,
				       selection = selsKs4e[key])
	
	
mystream = StrippingStream(name="KS04e",Lines = strilines.values())

sc = StrippingConf( Streams = [ mystream ],
                    MaxCandidates = -1,
                    TESPrefix = 'Strip'
                    )

mystream.sequence().IgnoreFilterPassed = False # so we filter events by stripping selected lines


#
## Write DST
#
enablePacking = True
dstExtension = ".dst"

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default' : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default' : stripDSTStreamConf(pack=enablePacking,
                                   selectiveRawEvent=False)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='MCMatch',
			  SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41452811)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
## DaVinci Configuration
#

DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence([myseq])
DaVinci().appendToMainSequence([ MatchedElectrons_seq[key] for key in MatchedElectrons_seq ])
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([ stck ])
DaVinci().appendToMainSequence([ dstWriter.sequence() ])


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60



