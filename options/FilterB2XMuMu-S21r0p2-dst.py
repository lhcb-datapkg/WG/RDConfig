"""
Stripping Filtering file for B2XMuMu S21r0p2 line, DST output
@author Vitalii Lisovskyi
@date   2020-03-03
"""

#stripping version
stripping='stripping21r0p2'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive




B2XMuMu = {
    'NAME'       : 'B2XMuMu',
    'BUILDERTYPE' : 'B2XMuMuConf',
    'CONFIG'     :
    {
    'RelatedInfoTools'      : [
                       {"Type": "RelInfoBKstarMuMuBDT",
            "Variables": ['MU_SLL_ISO_1', 'MU_SLL_ISO_2'],
            "Location": "KSTARMUMUVARIABLES"
             },
            {"Type": "RelInfoBKstarMuMuHad",
            "Variables": ['K_SLL_ISO_HAD', 'PI_SLL_ISO_HAD' ],
            "Location": "KSTARMUMUVARIABLES2"
            },
    {"Type" : "RelInfoConeVariables"
     , "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM','CONEPT','CONEP','CONEPASYM','CONEDELTAETA','CONEDELTAPHI']
     , "Location"  : "ConeIsoInfo" }
   ,{"Type" : "RelInfoConeIsolation"
     ,'ConeSize'         : 1.0
     ,'Variables' : [ 'CC_ANGLE', 'CC_MULT', 'CC_SPT', 'CC_VPT', 'CC_PX', 'CC_PY', 'CC_PZ', 'CC_PASYM', 'CC_PTASYM', 'CC_PXASYM', 'CC_PYASYM', 'CC_PZASYM', 'CC_DELTAETA', 'CC_DELTAPHI', 'CC_IT',  'CC_MAXPT_Q', 'CC_MAXPT_PT', 'CC_MAXPT_PX', 'CC_MAXPT_PY', 'CC_MAXPT_PZ', 'CC_MAXPT_PE',  'NC_ANGLE', 'NC_MULT', 'NC_SPT', 'NC_VPT', 'NC_PX', 'NC_PY', 'NC_PZ', 'NC_PASYM', 'NC_PTASYM', 'NC_PXASYM', 'NC_PYASYM', 'NC_PZASYM', 'NC_DELTAETA', 'NC_DELTAPHI', 'NC_IT',  'NC_MAXPT_PT', 'NC_MAXPT_PX', 'NC_MAXPT_PY', 'NC_MAXPT_PZ' ]
     ,'Location':'ConeIsoInfoCCNC'}
   ,{'Type' : 'RelInfoVertexIsolation'
     ,"Variables" : ['VTXISONUMVTX', 'VTXISODCHI2ONETRACK', 'VTXISODCHI2MASSONETRACK','VTXISODCHI2TWOTRACK','VTXISODCHI2MASSTWOTRACK']
     ,'Location':'VtxIsoInfo'}
   ,{'Type': 'RelInfoVertexIsolationBDT'
     , 'Location':'VtxIsoBDTInfo' }
    #,  {"Type" : "RelInfoBs2MuMuIsolations"
    #, "Variables" : ['BSMUMUCDFISO', 'BSMUMUOTHERBMAG', 'BSMUMUOTHERBANGLE', 'BSMUMUOTHERBBOOSTMAG', 'BSMUMUOTHERBBOOSTANGLE', 'BSMUMUTRACKPLUSISO', 'BSMUMUTRACKMINUSISO', 'BSMUMUOTHERBTRACKS']
    #, "Location"  : "BSMUMUVARIABLES"
    #  }
    ]
    , 'KpiVXCHI2NDOF'      : 8.0           # dimensionless
    , 'MuonPID'            : -3.0          # dimensionless
    , 'DimuonUPPERMASS'    : 7100.0        # MeV
    , 'Pi0MINPT'           : 800.0         # MeV
    , 'Pi0ForOmegaMINPT'   : 800.0         #was 500.0         # MeV
    #, 'DplusLOWERMASS'     : 1600.0        # MeV #no longer used
    #, 'DplusUPPERMASS'     : 2300.0        # MeV
    , 'KstarplusWINDOW'    : 300.0         # MeV
    , 'KsWINDOW'           : 30.0          # MeV
    , 'LambdaWINDOW'       : 30.0          # MeV
    , 'LongLivedPT'        : 0.0           # MeV , used to be 500.0 MeV
    , 'LongLivedTau'        : 2            # ps

    # A1 cuts
    , 'A1_Comb_MassLow'  :    0.0
    , 'A1_Comb_MassHigh' : 4200.0 #4200 in S28 #was 5550.0
    , 'A1_MassLow'       :    0.0
    , 'A1_MassHigh'      : 4000.0 #4000 #was 5500.0
    , 'A1_MinIPCHI2'     :    4.0
    , 'A1_FlightChi2'    :   36.0 #36.0 #was 25.0
    , 'A1_VtxChi2'       :    8.0 #8.0 #was 10.0
    , 'A1_Dau_MaxIPCHI2' :   16.0 #16.0 #was 9.0
    # From Bd2KstarMuMu line
    ,'UseNoPIDsHadrons'          : True,

    # B cuts
    'B_Comb_MassLow'      : 4600.0,
    'B_Comb_MassHigh'     : 7100.0,
    'B_MassLow'           : 4700.0,
    'B_MassHigh'          : 7000.0,
    'B_VertexCHI2'        :    8.0,
    'B_IPCHI2'            :   16.0,
    'B_DIRA'              : 0.9999,
    'B_FlightCHI2'        :   64.0,#64 #was 121.0
    'B_Dau_MaxIPCHI2'     :    9.0,

    # Daughter cuts
    'Dau_VertexCHI2'      :   12.0,
    'Dau_DIRA'            :   -0.9,

    # Kstar cuts
    'Kstar_Comb_MassLow'  :    0.0,
    'Kstar_Comb_MassHigh' : 6200.0,
    'Kstar_MassLow'       :    0.0,
    'Kstar_MassHigh'      : 6200.0,
    'Kstar_MinIPCHI2'     :    0.0,
    'Kstar_FlightChi2'    :    16.0,#S28 16.0 #was 9.0
    'Kstar_Dau_MaxIPCHI2' :    6.0,#S28 6.0 #was 9.0

    # Omega cuts
    'Omega_MassWin'       :   400, #MeV
    'Omega_CombMassWin'   :   400, #MeV
    'OmegaChi2Prob'       :  1e-5,  # dimensionless

    #K1->OmegaK cuts
    'K12OmegaK_MassLow'   :   300, #MeV
    'K12OmegaK_MassHigh'  :  2100, #MeV
    'K12OmegaK_CombMassLow'   :   400, #MeV
    'K12OmegaK_CombMassHigh'  :  2000, #MeV
    'K12OmegaK_VtxChi2'   : 8 ,

    #hyperon cuts
    'HyperonWindow' : 50.0, #new
    'HyperonCombWindow' : 65.0,
    'HyperonMaxDocaChi2' : 20.0,

    # JPsi (dimu) cuts
    'Dimu_FlightChi2'     :   9.0,
    'Dimu_Dau_MaxIPCHI2'  :   6.0,#6.0 #was 9.0

    # Track cuts
    'Track_GhostProb'     :    0.5,

    # Hadron cuts
    'Hadron_MinIPCHI2'    :    6.0,

    # Muon cuts
    'Muon_MinIPCHI2'      :    6.0,#6.0 #was 9.0
    'Muon_IsMuon'         :    True,
    #'MuonNoPIDs_PIDmu'    :    0.0, #not used

    # Wrong sign combinations
    'DimuonWS'            :   True,
    'HadronWS'            :   False,#False in S28 #was True

    # GEC
    'SpdMult'             :  600,

    'HLT2_FILTER' : None ,
    'HLT1_FILTER' : None ,
    'L0DU_FILTER' : None ,

    'DECAYS'              :  ["B0 -> J/psi(1S) phi(1020)",
                              "[B0 -> J/psi(1S) K*(892)0]cc",
                              "B0 -> J/psi(1S) rho(770)0",
                              "[B+ -> J/psi(1S) rho(770)+]cc",
                              "B0 -> J/psi(1S) f_2(1950)",
                              "B0 -> J/psi(1S) KS0",
                              "[B0 -> J/psi(1S) D~0]cc",
                              "[B+ -> J/psi(1S) K+]cc",
                              "[B+ -> J/psi(1S) pi+]cc",
                              "[B+ -> J/psi(1S) K*(892)+]cc",
                              "[B+ -> J/psi(1S) D+]cc",
                              "[B+ -> J/psi(1S) D*(2010)+]cc",
                              "[Lambda_b0 -> J/psi(1S) Lambda0]cc",
                              "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc",
                              "B0 -> J/psi(1S) pi0",
                              "[B+ -> J/psi(1S) a_1(1260)+]cc",
                              "[B+ -> J/psi(1S) K_1(1270)+]cc",
                              "[B+ -> J/psi(1S) K_2(1770)+]cc",
                              "B0 -> J/psi(1S) K_1(1270)0",
                              "[B+ -> J/psi(1S) K_1(1400)+]cc",
                              "B0 -> J/psi(1S) K_1(1400)0",
                              #
                              "[Xi_b- -> J/psi(1S) Xi-]cc",#new
                              "[Omega_b- -> J/psi(1S) Omega-]cc",
                              "B0 -> J/psi(1S) f_1(1285)",
                              "B0 -> J/psi(1S) omega(782)"
                             ]

    },
    'WGs'     : [ 'RD' ],
    'STREAMS' : ['Leptonic']
    }


config = {'B2XMuMu' : B2XMuMu}
#get the configuration dictionary from the database
#config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)


#
# Merge into one stream and run in filter mode
#
AllStreams = StrippingStream("B2XMuMu.Strip")

linesToAdd = []
for stream in streams:
    if 'Leptonic' in stream.name():
        for line in stream.lines:
            if 'B2XMuMu' in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so we filter events by stripping selected lines

#
# Configure the dst writers for the output
#
enablePacking = True
#enablePacking = False

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements
                                       )


#############################################################################
#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements(stripPrefix='Strip')
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf(stripPrefix='Strip')
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='RD',
                          SelectionSequences = sc.activeStreams()
                          )



# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x39162102)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().DataType = "2012"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

# Save info of rejection factor in the filtering
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]

#from GaudiConf import IOHelper

# Use the local input data
#IOHelper().inputFiles([
#'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00027345/0000/00027345_00000027_1.allstreams.dst'
#], clear=True)
