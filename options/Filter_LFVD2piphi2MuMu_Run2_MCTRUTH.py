from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 5 ],
				"CloneDistCut" : [5000, 9e+99 ] }


from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive


from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand
from Configurables import FilterDesktop
from Configurables import LoKiSvc

# truth matching standard particle


_stdMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
_muonFilter = FilterDesktop('muonFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
			    Code = " (mcMatch('[mu+]cc'))")

MuonFilterSel = Selection(name = 'MuonFilterSel',
			  Algorithm = _muonFilter,
			  RequiredSelections = [ _stdMuons ])



_stdPions = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
_pionFilter = FilterDesktop('pionFilter',
                            Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
                            Code = "(mcMatch('[pi+]cc'))")

PionFilterSel = Selection(name = 'PionFilterSel',
                          Algorithm = _pionFilter,
                          RequiredSelections = [ _stdPions ])


# truthmach phi

_makePhi = CombineParticles("make_Phi",
                            DecayDescriptor = "[phi(1020) -> mu+ mu-]cc",
                            CombinationCut = "(ADAMASS('phi(1020)') < 400 *MeV)",
                            MotherCut = "mcMatch( '[phi(1020) => mu+ mu- ]CC' )" )

selPhi =  Selection("SelPhi",
                    Algorithm = _makePhi,
                    RequiredSelections = [MuonFilterSel] )

# truth matching D


_makeD = CombineParticles("make_D",
			   DecayDescriptor = "[D+ -> phi(1020) pi+  ]cc",
			   CombinationCut =  "(ADAMASS('D-') < 600 *MeV)",
			   MotherCut = "mcMatch( '[ D+ => phi(1020) pi+ ]CC' ) | mcMatch( '[ D- => phi(1020) pi-  ]CC' )" )

selD = Selection("SelD",
		  Algorithm = _makeD,
		  RequiredSelections = [ PionFilterSel, selPhi] )
seq = SelectionSequence("MCTrue", TopSelection=selD )
			   


#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
				      stripDSTStreamConf,
				      stripDSTElements
				      )

SelDSTWriterElements = {
	'default'               : stripDSTElements(pack=enablePacking)
	     }

SelDSTWriterConf = {
	'default'               : stripDSTStreamConf(pack=enablePacking)
	     }

dstWriter = SelDSTWriter( "MyDSTWriter",
			                             StreamConf = SelDSTWriterConf,
			                             MicroDSTElements = SelDSTWriterElements,
			                             OutputFileSuffix ='Filtered_LFVD2piphi2MuMu_Run2',
			                             SelectionSequences = [seq]
			                             )

from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ seq.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

# TO DELETE
