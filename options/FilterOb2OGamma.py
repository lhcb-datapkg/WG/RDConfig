"""
MC-matching/reconstruction filtering for Hlt2 filtering requests
This script selects only those particle with an associated MCParticle.
This ensures the reconstructibility of the Ob2OGamma candidates.
@author Luis Miguel Garcia Martin
@date   2020-10-13
"""

########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, MergedSelection

#from CommonMCParticles import StandardMCLambda0, StandardMCPhotons, StandardMCKaons


def FilterBasic(name, code, loc):
    _Filter = FilterDesktop(
        '{name}_Filter'.format(name=name),
        Code=code,
        Preambulo=[
            "from LoKiPhysMC.decorators import *",
            "from PartProp.Nodes import CC"
        ])

    _input = DataOnDemand(Location='Phys/{loc}/Particles'.format(loc=loc))

    return Selection(
        '{name}_Sel'.format(name=name),
        Algorithm=_Filter,
        RequiredSelections=[_input])


###############################

_p_L = FilterBasic('MCProton_L', "(mcMatch('[p+]cc'))", 'StdNoPIDsProtons')
_p_D = FilterBasic('MCProton_D', "(mcMatch('[p+]cc'))", 'StdNoPIDsDownProtons')
_pi_L = FilterBasic('MCPion_L', "(mcMatch('[pi+]cc'))", 'StdNoPIDsPions')
_pi_D = FilterBasic('MCPion_D', "(mcMatch('[pi+]cc'))", 'StdNoPIDsDownPions')
_k_L = FilterBasic('MCKaon_L', "(mcMatch('[K+]cc'))", 'StdNoPIDsKaons')
_k_D = FilterBasic('MCKaon_D', "(mcMatch('[K+]cc'))", 'StdNoPIDsDownKaons')
_gamma = FilterBasic('MCPhoton', "(mcMatch('gamma'))", 'StdLoosePhotons')

_L02PPi = CombineParticles(
    "L0ToPPi",
    DecayDescriptor="[Lambda0 -> p+ pi-]cc",
    MotherCut="(mcMatch('[Lambda0 ==> p+ pi-]CC'))",
    Preambulo=[
        "from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"
    ])

_l0_LL = Selection(
    "Lambda_LL", Algorithm=_L02PPi, RequiredSelections=[_p_L, _pi_L])

_l0_DD = Selection(
    "Lambda_DD", Algorithm=_L02PPi, RequiredSelections=[_p_D, _pi_D])
#
# Omega- -> L0 K-
#
matchO2L0K = "(mcMatch('[Omega-  ==> Lambda0 K-]CC'))"

_O2L0K = CombineParticles("O2L0K")
_O2L0K.DecayDescriptor = "[Omega-  -> Lambda0 K-]cc"
_O2L0K.MotherCut = matchO2L0K
_O2L0K.Preambulo = [
    "from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"
]

SelO2L0K_LLL = Selection(
    "SelO2L0K_LLL", Algorithm=_O2L0K, RequiredSelections=[_l0_LL, _k_L])

SelO2L0K_DDL = Selection(
    "SelO2L0K_DDL", Algorithm=_O2L0K, RequiredSelections=[_l0_DD, _k_L])

SelO2L0K_DDD = Selection(
    "SelO2L0K_DDD", Algorithm=_O2L0K, RequiredSelections=[_l0_DD, _k_D])

SelO2L0K = MergedSelection(
    'SelO2L0K', RequiredSelections=[SelO2L0K_LLL, SelO2L0K_DDL, SelO2L0K_DDD])

#
# Omega_b- -> Omega- Gamma
#

matchOb2Ogamma = "(mcMatch('[Xi_b-  ==> Omega- gamma]CC'))"

_Ob2Ogamma = CombineParticles("Ob2Ogamma")
_Ob2Ogamma.DecayDescriptor = "[Xi_b-  -> Omega- gamma]cc"
_Ob2Ogamma.DaughtersCuts = {"gamma": "PT>1.5*GeV"}
_Ob2Ogamma.ParticleCombiners = {'': 'ParticleAdder'}
_Ob2Ogamma.MotherCut = matchOb2Ogamma
_Ob2Ogamma.Preambulo = [
    "from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"
]

SelOb2Ogamma = Selection(
    "SelOb2OGamma",
    Algorithm=_Ob2Ogamma,
    RequiredSelections=[SelO2L0K, _gamma])

SeqOb2Ogamma = SelectionSequence('MCFilter', TopSelection=SelOb2Ogamma)

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter, stripDSTStreamConf,
                                      stripDSTElements)
SelDSTWriterElements = {'default': stripDSTElements(pack=enablePacking)}
SelDSTWriterConf = {'default': stripDSTStreamConf(fileExtension=dstExtension)}
dstWriter = SelDSTWriter(
    "MyDSTWriter",
    StreamConf=SelDSTWriterConf,
    MicroDSTElements=SelDSTWriterElements,
    OutputFileSuffix='SelOb2OGamma',
    SelectionSequences=[SeqOb2Ogamma],
)
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
#DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1  # Number of events
DaVinci().appendToMainSequence([SeqOb2Ogamma.sequence(), dstWriter.sequence()])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60
