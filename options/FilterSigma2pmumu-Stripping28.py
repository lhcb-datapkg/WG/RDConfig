"""
Stripping Filtering file for Sigma->pmumu S28 line
@author Jessica Prisciandaro
@date   2017-07-13
"""


from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"


#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

stripping = 'stripping28'

archive = strippingArchive(stripping)
config  = strippingConfiguration(stripping)
streams = buildStreams(stripping = config, archive = archive)

lines = []
for stream in streams:
    if 'Leptonic' in stream.name():
        for line in stream.lines:
            if 'RareStrangeSigmaPMuMuLine' in line.name():
                line._prescale = 1.0
                lines.append(line)

AllStreams = StrippingStream("RareStrangeSigmaPMuMu.Strip")
AllStreams.appendLines(lines)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    #HDRLocation = 'TempStrip', # TO BE REMOVED FOR MC PRODUCTION  
                    TESPrefix = 'Strip'
                    )

# So that we get only selected events written out
AllStreams.sequence().IgnoreFilterPassed = False

#
# Configuration of SelDSTWriter
#
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTElements,
                                      stripDSTStreamConf
                                      )

enablePacking = True

SelDSTWriterElements = {
    'default' : stripDSTElements(pack=enablePacking)
    }


SelDSTWriterConf = {
    'default' : stripDSTStreamConf(pack=enablePacking)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Sigma2pmumu',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = '2016'
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = 'DVHistos.root'
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

