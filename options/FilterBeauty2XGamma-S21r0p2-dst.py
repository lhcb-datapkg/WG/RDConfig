"""
Stripping21r0p2 filtered production for streams: 'B2XGammaExclTDCPV', 'B2XGamma'
@author Biplab Dey
@date   2020-04-02, update 2020-06-23 CALO+PROTO ReProcessing
"""
'''
#------Remove---------
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#---------------------
'''

#stripping version
stripping='stripping21r0p2'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = []
streams.append( buildStream(stripping=config, streamName='Leptonic', archive=archive) )
streams.append( buildStream(stripping=config, streamName='BhadronCompleteEvent', archive=archive) )

AllStreams = StrippingStream("Beauty2XGamma.Strip")

linesToAdd = []
for stream in streams:
    for line in stream.lines:
        if 'Beauty2XGamma' in line.name(): linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip' )


AllStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,
#                                                 selectiveRawEvent=True,
                                                 fileExtension='.dst')
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x39162102)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

# https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/FilteredSimulationProduction
# for Stripping21(rXpY) [2011+2012], 24r2[2015] or 28r2 [2016] productions, you must add these lines to your basic options: 
# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"]
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs
#

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"

#------Remove---------
#DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
#---------------------
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60



#######################################################################
#------Remove---------
'''
DaVinci().DataType = "2012"
# Save info of rejection factor in the filtering
from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output2012.txt"
DaVinci().MoniSequence += [ DumpFSR() ]

from GaudiConf import IOHelper
# Use the local input data

# S21 Bd->K*gamma
#evt+std://MC/2012/11102202/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09b/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/ALLSTREAMS.DST
IOHelper().inputFiles([
    '/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00067225/0000/00067225_00000006_5.AllStreams.dst',
    '/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00067225/0000/00067225_00000011_5.AllStreams.dst',
    '/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00067225/0000/00067225_00000012_5.AllStreams.dst',
    '/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00067225/0000/00067225_00000016_5.AllStreams.dst',
    ], clear=True)
'''


