from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 5 ],
				"CloneDistCut" : [5000, 9e+99 ] }


from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive


from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand
from Configurables import FilterDesktop
from Configurables import LoKiSvc

# truth matching standard particle

_stdPions = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
_pionFilter = FilterDesktop('pionFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
			    Code = "(mcMatch('[pi+]cc'))")

PionFilterSel = Selection(name = 'PionFilterSel',
			  Algorithm = _pionFilter,
			  RequiredSelections = [ _stdPions ])



_stdKaons = DataOnDemand(Location = "Phys/StdAllNoPIDsKaons/Particles")
_kaonFilter = FilterDesktop('kaonFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
			    Code = "(mcMatch('[K+]cc'))" )
KaonFilterSel = Selection(name = 'KaonFilterSel',
			  Algorithm = _kaonFilter,
			  RequiredSelections = [ _stdKaons])



_stdMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
_muonFilter = FilterDesktop('muonFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
			    Code = " (mcMatch('[mu+]cc'))")

MuonFilterSel = Selection(name = 'MuonFilterSel',
			  Algorithm = _muonFilter,
			  RequiredSelections = [ _stdMuons ])


# truth matching K*



_makeKstar = CombineParticles("make_Kstar",
			   DecayDescriptor = "[K*(892)0 -> K+ pi-]cc",
			   CombinationCut = "(ADAMASS('K*(892)0') < 400 *MeV)",
			   MotherCut = "mcMatch( '[K*(892)0 => K+ pi-]CC' )" )

selKstar =  Selection("SelKstar",
		      Algorithm = _makeKstar,
		      RequiredSelections = [KaonFilterSel, PionFilterSel])


_makeB0 = CombineParticles("make_B0",
			   DecayDescriptor = "[B0 -> K*(892)0 mu+ mu- ]cc",
			   CombinationCut =  "(ADAMASS('B0') < 500 *MeV)",
			   MotherCut = "mcMatch( '[ B0 => K*(892)0 mu+ mu- ]CC' ) | mcMatch( '[ B0 => K*(892)~0 mu- mu+ ]CC' )" )

selB0 = Selection("SelB0",
		  Algorithm = _makeB0,
		  RequiredSelections = [selKstar, MuonFilterSel] )
seq = SelectionSequence("MCTrue", TopSelection=selB0 )
			   


#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
				      stripDSTStreamConf,
				      stripDSTElements
				      )

SelDSTWriterElements = {
	'default'               : stripDSTElements(pack=enablePacking)
	     }

SelDSTWriterConf = {
	'default'               : stripDSTStreamConf(pack=enablePacking)
	     }

dstWriter = SelDSTWriter( "MyDSTWriter",
			                             StreamConf = SelDSTWriterConf,
			                             MicroDSTElements = SelDSTWriterElements,
			                             OutputFileSuffix ='Filtered_Kstmm_Run1',
			                             SelectionSequences = [seq]
			                             )

from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ seq.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

