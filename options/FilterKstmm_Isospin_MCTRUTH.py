from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 5 ],
				"CloneDistCut" : [5000, 9e+99 ] }


from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, MergedSelection
from Configurables import FilterDesktop

# truth matching standard particle

_stdPions = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
_pionFilter = FilterDesktop('pionFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
			    Code = "(mcMatch('[pi+]cc'))")

PionFilterSel = Selection(name = 'PionFilterSel',
			  Algorithm = _pionFilter,
			  RequiredSelections = [ _stdPions ])


# KS matching
_ksdd = DataOnDemand(Location = 'Phys/StdLooseKsDD/Particles')
_ksll = DataOnDemand(Location = 'Phys/StdVeryLooseKsLL/Particles')
_filter_ks = FilterDesktop('KSFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
			    Code = "(mcMatch('[KS0]cc'))" )      
_selksdd = Selection("KS0DDFilterSel",
                    RequiredSelections = [ _ksdd ] ,
                    Algorithm = _filter_ks)
_selksll = Selection("KS0LLFilterSel",
                    RequiredSelections = [ _ksll ] ,
                    Algorithm = _filter_ks)

KSFilterSel = MergedSelection("KshortFilterSelection",
                        RequiredSelections = [ _selksdd, _selksll ])


# Muon matching
_stdMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
_muonFilter = FilterDesktop('muonFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
			    Code = " (mcMatch('[mu+]cc'))")

MuonFilterSel = Selection(name = 'MuonFilterSel',
			  Algorithm = _muonFilter,
			  RequiredSelections = [ _stdMuons ])


# truth matching K*

_makeKstar = CombineParticles("make_Kstar",
			   DecayDescriptor = "[K*(892)+ -> KS0 pi+]cc",
			   CombinationCut = "(ADAMASS('K*(892)+') < 400 *MeV)",
			   MotherCut = "mcMatch( '[K*(892)+ => KS0 pi+]CC' )" )

selKstar =  Selection("SelKstar",
		      Algorithm = _makeKstar,
		      RequiredSelections = [KSFilterSel, PionFilterSel])


_makeBplus = CombineParticles("make_Bplus",
			   DecayDescriptor = "[B+ -> K*(892)+ mu+ mu- ]cc",
			   CombinationCut =  "(ADAMASS('B0') < 500 *MeV)",
			   MotherCut = "mcMatch( '[ B+ => K*(892)+ mu+ mu- ]CC' ) | mcMatch( '[ B- => K*(892)- mu- mu+ ]CC' )" )

selBplus = Selection("SelBplus",
		  Algorithm = _makeBplus,
		  RequiredSelections = [selKstar, MuonFilterSel] )
seq = SelectionSequence("MCTrue", TopSelection=selBplus )
			   


#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
				      stripDSTStreamConf,
				      stripDSTElements
				      )

SelDSTWriterElements = {
	'default'               : stripDSTElements(pack=enablePacking)
	     }

SelDSTWriterConf = {
	'default'               : stripDSTStreamConf(pack=enablePacking)
	     }

dstWriter = SelDSTWriter( "MyDSTWriter",
			                             StreamConf = SelDSTWriterConf,
			                             MicroDSTElements = SelDSTWriterElements,
			                             OutputFileSuffix ='Filtered_KstmmIsospin',
			                             SelectionSequences = [seq]
			                             )

from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ seq.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

