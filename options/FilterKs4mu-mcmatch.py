"""
MCMatch Filtering file for Ks->4mu
@author Adrian CasaisVidal, Xabier Cid Vidal, Carla Marin
@date   2018-09-26
"""

from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from Configurables import ChargedProtoParticleMaker,ChargedPP2MC,NoPIDsParticleMaker,EventSelector
from PhysSelPython.Wrappers import Selection, SelectionSequence

from StandardParticles import StdAllNoPIDsMuons

from CommonParticles.Utils import *

#
## Making VELO/Upstream tracks available
#


myprotos = ChargedProtoParticleMaker("MyProtoParticles",
                                     Inputs = ["Rec/Track/Best"],
                                     Output = "Rec/ProtoP/MyProtoParticles")

protop_locations = [myprotos.Output]
charged = ChargedPP2MC("myprotos")
charged.InputData = protop_locations
myseq = GaudiSequencer("myseq")
myseq.Members +=[myprotos,charged]



                

algorithm_velo =  NoPIDsParticleMaker ( 'StdNoPIDsVeloMuonsLocal',
                                   DecayDescriptor = 'Muon' ,
                                   Particle = 'muon',
                                   AddBremPhotonTo= [],
                                   Input = myprotos.Output)

algorithm_upstream =  NoPIDsParticleMaker ( 'StdNoPIDsUpMuonsLocal',
                                   DecayDescriptor = 'Muon' ,
                                   Particle = 'muon',
                                   AddBremPhotonTo= [],
                                   Input = myprotos.Output)



selector_velo = trackSelector ( algorithm_velo,trackTypes = [ "Velo" ]  )
selector_upstream = trackSelector ( algorithm_upstream,trackTypes = [ "Upstream" ]  )
locations_velo = updateDoD ( algorithm_velo )
locations_upstream = updateDoD ( algorithm_upstream )


from PhysSelPython.Wrappers import AutomaticData

StdNoPIDsVeloMuons = AutomaticData(Location = "Phys/StdNoPIDsVeloMuonsLocal/Particles")
StdNoPIDsUpMuons = AutomaticData(Location = "Phys/StdNoPIDsUpMuonsLocal/Particles")
                                      

_sels = {"Long":StdAllNoPIDsMuons,"Velo":StdNoPIDsVeloMuons,"Up":StdNoPIDsUpMuons}
MatchedMuons={}
MatchedMuons_seq={}
locations = {}

#
## Matched electrons locations
#

for key in ["Long","Velo","Up"]:
    if key == "Velo":
	    _loc = "'/Event/Relations/Rec/ProtoP/MyProtoParticles'"
	    _code = "( mcMatch('KS0 => ^mu+ mu- mu+ mu-', %s )) | (mcMatch('KS0 => mu+ ^mu- mu+ mu-', %s )) | (mcMatch('KS0 => mu+ mu- ^mu+ mu-', %s )) | (mcMatch('KS0 => mu+ mu- mu+ ^mu-', %s ))"%(_loc,_loc,_loc,_loc)
    if key == "Up":
	    _loc = "'/Event/Relations/Rec/ProtoP/MyProtoParticles'"
            
	    _code = "( mcMatch('KS0 => ^mu+ mu- mu+ mu-', %s )) | (mcMatch('KS0 => mu+ ^mu- mu+ mu-', %s )) | (mcMatch('KS0 => mu+ mu- ^mu+ mu-', %s )) | (mcMatch('KS0 => mu+ mu- mu+ ^mu-', %s ))"%(_loc,_loc,_loc,_loc)
    if key == "Long":
	    _code = "( mcMatch('KS0 => ^mu+ mu- mu+ mu-')) | (mcMatch('KS0 => mu+ ^mu- mu+ mu-')) | (mcMatch('KS0 => mu+ mu- ^mu+ mu-')) | (mcMatch('KS0 => mu+ mu- mu+ ^mu-'))"
    
    preambulo =[ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ]
    _Filter = FilterDesktop(Preambulo = preambulo,Code = _code)
    
    MatchedMuons[key]  = Selection("Matched"+key+"Muons",Algorithm = _Filter, RequiredSelections = [_sels[key]]   )
				       
    MatchedMuons_seq[key] = SelectionSequence("Matched"+key+"MuonsSequence", TopSelection=MatchedMuons[key])
     

#
## Setting up combinations
#

combs = {"LL":"( (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 4 ))",
         "UU":"( (ANUM(  ( TRTYPE == 4 ) &  ( ABSID == 'mu-' ) ) == 2)  & (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 2) )",
         "VV":"( (ANUM(  ( TRTYPE == 1 ) &  ( ABSID == 'mu-' ) ) == 2)  & (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 2) ) ",
         "LU":"( (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 3)  & (ANUM(  ( TRTYPE == 4 ) &  ( ABSID == 'mu-' ) ) == 1) )",
         "LV":"( (ANUM(  ( TRTYPE == 1 ) &  ( ABSID == 'mu-' ) ) == 1)  & (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 3) )",
         "UV":"( (ANUM(  ( TRTYPE == 4 ) &  ( ABSID == 'mu-' ) ) == 1)  & (ANUM(  ( TRTYPE == 1 ) &  ( ABSID == 'mu-' ) ) == 1) & (ANUM(  ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 2)  )"}



selsKs4mu={}
seqsks24mu={}
Ks24mu={}
for name in combs:
    Ks24mu[name] = CombineParticles("TrackSel"+name+"_Ks24mu")

    if "V" in name:
        Ks24mu[name].DecayDescriptors = ["KS0 -> mu+ mu- mu+ mu-","KS0 -> mu+ mu- mu+ mu+","KS0 -> mu+ mu- mu- mu-"]
	
    else:
        Ks24mu[name].DecayDescriptor = "KS0 -> mu+ mu- mu+ mu-"
    Ks24mu[name].Preambulo=["from LoKiPhysMC.decorators import *",
                               "from LoKiPhysMC.functions import mcMatch"]
     

    
    Ks24mu[name].CombinationCut = combs[name]
    Ks24mu[name].MotherCut = "ALL"
    


    Ks24mu[name].Inputs =['Phys/MatchedLongMuons/Particles']
    if "U" in name:
	    Ks24mu[name].Inputs+=['Phys/MatchedUpMuons/Particles']
    if "V" in name:
	    Ks24mu[name].Inputs+=['Phys/MatchedVeloMuons/Particles']
    

    if name=="UV":
	    selsKs4mu[name]= Selection("SelKs24mu_"+name,
			      Algorithm = Ks24mu[name],
                              
			      RequiredSelections=[AutomaticData(Location = 'Phys/MatchedLongMuons/Particles'),
    			       			  AutomaticData(Location ='Phys/MatchedUpMuons/Particles'),
    			       			  AutomaticData(Location = 'Phys/MatchedVeloMuons/Particles')])
	    
    elif "U" in name:
	    selsKs4mu[name]= Selection("SelKs24mu_"+name,
			      Algorithm = Ks24mu[name],
                              
			      RequiredSelections=[AutomaticData(Location = 'Phys/MatchedLongMuons/Particles'),
    			       			  AutomaticData(Location ='Phys/MatchedUpMuons/Particles')
    			       			  ])
    elif "V" in name:
	    selsKs4mu[name]= Selection("SelKs24mu_"+name,
			      Algorithm = Ks24mu[name],
                              
			      RequiredSelections=[AutomaticData(Location = 'Phys/MatchedLongMuons/Particles'),
    			       			  AutomaticData(Location ='Phys/MatchedVeloMuons/Particles')
    			       			  ])
    else: 
	    selsKs4mu[name]= Selection("SelKs24mu_"+name,
			      Algorithm = Ks24mu[name],
                              
			      RequiredSelections=[AutomaticData(Location = 'Phys/MatchedLongMuons/Particles')
    			       			  ])
				    
    
                       

#
## Merging streams
#

from StrippingConf.Configuration import StrippingConf
from StrippingConf.StrippingLine import StrippingLine
from StrippingConf.StrippingStream import StrippingStream

strilines = {}
for key in selsKs4mu:
	strilines[key] = StrippingLine("line"+key,
				       prescale  = 1,
				       postscale = 1,
				       selection = selsKs4mu[key])
	
	
mystream = StrippingStream(name="KS04mu",Lines = strilines.values())

sc = StrippingConf( Streams = [ mystream ],
                    MaxCandidates = -1,
                    TESPrefix = 'Strip'
                    )

mystream.sequence().IgnoreFilterPassed = False # so we filter events by stripping selected lines


#
# Write DST
#
enablePacking = True
dstExtension = ".dst"

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default' : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default' : stripDSTStreamConf(pack=enablePacking,
                                   selectiveRawEvent=False)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='MCMatch',
			  SelectionSequences = sc.activeStreams()
                          )




# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41452811)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number



#
# DaVinci Configuration
#

DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence([myseq])
DaVinci().appendToMainSequence([ MatchedMuons_seq[key] for key in MatchedMuons_seq ])
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([ stck ])
DaVinci().appendToMainSequence([ dstWriter.sequence() ])



# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60



