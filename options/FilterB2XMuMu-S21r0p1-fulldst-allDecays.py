"""
Stripping Filtering file for B2XMuMu S21r0p1 line of fullDST, but including all decays like in S21
(mainly copied from FilterB2XMuMu-S26-mdst.py)
@author Michel De Cian
@date   2017-09-21
"""

#########################################

B2XMuMu = {
    "BUILDERTYPE" : "B2XMuMuConf",
    "CONFIG": { "A1_Comb_MassHigh": 5550.0,
                "A1_Comb_MassLow": 0.0,
                "A1_Dau_MaxIPCHI2": 9.0,
                "A1_FlightChi2": 25.0,
                "A1_MassHigh": 5500.0,
                "A1_MassLow": 0.0,
                "A1_MinIPCHI2": 4.0,
                "A1_VtxChi2": 10.0,
                "B_Comb_MassHigh": 7100.0,
                "B_Comb_MassLow": 4600.0,
                "B_DIRA": 0.9999,
                "B_Dau_MaxIPCHI2": 9.0,
                "B_FlightCHI2": 121.0,
                "B_IPCHI2": 16.0,
                "B_MassHigh": 7000.0,
                "B_MassLow": 4700.0,
                "B_VertexCHI2": 8.0,
                "DECAYS": [ "B0 -> J/psi(1S) phi(1020)",
                            "[B0 -> J/psi(1S) K*(892)0]cc",
                            "B0 -> J/psi(1S) rho(770)0",
                            "[B+ -> J/psi(1S) rho(770)+]cc",
                            "B0 -> J/psi(1S) f_2(1950)",
                            "B0 -> J/psi(1S) KS0",
                            "[B0 -> J/psi(1S) D~0]cc",
                            "[B+ -> J/psi(1S) K+]cc",
                            "[B+ -> J/psi(1S) pi+]cc",
                            "[B+ -> J/psi(1S) K*(892)+]cc",
                            "[B+ -> J/psi(1S) D+]cc",
                            "[B+ -> J/psi(1S) D*(2010)+]cc",
                            "[Lambda_b0 -> J/psi(1S) Lambda0]cc",
                            "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc",
                            "B0 -> J/psi(1S) pi0",
                            "[B+ -> J/psi(1S) a_1(1260)+]cc",
                            "[B+ -> J/psi(1S) K_1(1270)+]cc",
                            "[B+ -> J/psi(1S) K_2(1770)+]cc",
                            "B0 -> J/psi(1S) K_1(1270)0",
                            "[B+ -> J/psi(1S) K_1(1400)+]cc",
                            "B0 -> J/psi(1S) K_1(1400)0"],
                "Dau_DIRA": -0.9,
                "Dau_VertexCHI2": 12.0,
                "Dimu_Dau_MaxIPCHI2": 9.0,
                "Dimu_FlightChi2": 9.0,
                "DimuonUPPERMASS": 7100.0,
                "DimuonWS": True,
                "DplusLOWERMASS": 1600.0,
                "DplusUPPERMASS": 2300.0,
                "HLT1_FILTER": None,
                "HLT2_FILTER": None,
                "HadronWS": True,
                "Hadron_MinIPCHI2": 6.0,
                "K12OmegaK_CombMassHigh": 2000,
                "K12OmegaK_CombMassLow": 400,
                "K12OmegaK_MassHigh": 2100,
                "K12OmegaK_MassLow": 300,
                "K12OmegaK_VtxChi2": 10,
                "KpiVXCHI2NDOF": 9.0,
                "KsWINDOW": 30.0,
                "Kstar_Comb_MassHigh": 6200.0,
                "Kstar_Comb_MassLow": 0.0,
                "Kstar_Dau_MaxIPCHI2": 9.0,
                "Kstar_FlightChi2": 9.0,
                "Kstar_MassHigh": 6200.0,
                "Kstar_MassLow": 0.0,
                "Kstar_MinIPCHI2": 0.0,
                "KstarplusWINDOW": 300.0,
                "L0DU_FILTER": None,
                "LambdaWINDOW": 30.0,
                "LongLivedPT": 0.0,
                "LongLivedTau": 2,
                "MuonNoPIDs_PIDmu": 0.0,
                "MuonPID": -3.0,
                "Muon_IsMuon": True,
                "Muon_MinIPCHI2": 9.0,
                "OmegaChi2Prob": 1e-05,
                "Omega_CombMassWin": 200,
                "Omega_MassWin": 100,
                "Pi0ForOmegaMINPT": 500.0,
                "Pi0MINPT": 700.0,
                "RelatedInfoTools": [ { "Location": "ConeIsoInfo",
                                        "Type": "RelInfoConeVariables",
                                        "Variables": [ "CONEANGLE",
                                                       "CONEMULT",
                                                       "CONEPTASYM",
                                                       "CONEPT",
                                                       "CONEP",
                                                       "CONEPASYM",
                                                       "CONEDELTAETA",
                                                       "CONEDELTAPHI"
                                                       ]
                                        },
                                      { "ConeSize": 1.0,
                                        "Location": "ConeIsoInfoCCNC",
                                        "Type": "RelInfoConeIsolation",
                                        "Variables": [ "CC_ANGLE",
                                                       "CC_MULT",
                                                       "CC_SPT",
                                                       "CC_VPT",
                                                       "CC_PX",
                                                       "CC_PY",
                                                       "CC_PZ",
                                                       "CC_PASYM",
                                                       "CC_PTASYM",
                                                       "CC_PXASYM",
                                                       "CC_PYASYM",
                                                       "CC_PZASYM",
                                                       "CC_DELTAETA",
                                                       "CC_DELTAPHI",
                                                       "CC_IT",
                                                       "CC_MAXPT_Q",
                                                       "CC_MAXPT_PT",
                                                       "CC_MAXPT_PX",
                                                       "CC_MAXPT_PY",
                                                       "CC_MAXPT_PZ",
                                                       "CC_MAXPT_PE",
                                                       "NC_ANGLE",
                                                       "NC_MULT",
                                                       "NC_SPT",
                                                       "NC_VPT",
                                                       "NC_PX",
                                                       "NC_PY",
                                                       "NC_PZ",
                                                       "NC_PASYM",
                                                       "NC_PTASYM",
                                                       "NC_PXASYM",
                                                       "NC_PYASYM",
                                                       "NC_PZASYM",
                                                       "NC_DELTAETA",
                                                       "NC_DELTAPHI",
                                                       "NC_IT",
                                                       "NC_MAXPT_PT",
                                                       "NC_MAXPT_PX",
                                                       "NC_MAXPT_PY",
                                                       "NC_MAXPT_PZ" ]
                                        },
                                      { "Location": "VtxIsoInfo",
                                        "Type": "RelInfoVertexIsolation",
                                        "Variables": [ "VTXISONUMVTX",
                                                       "VTXISODCHI2ONETRACK",
                                                       "VTXISODCHI2MASSONETRACK",
                                                       "VTXISODCHI2TWOTRACK",
                                                       "VTXISODCHI2MASSTWOTRACK"
                                                       ]
                                        },
                                      { "Location": "VtxIsoBDTInfo",
                                        "Type": "RelInfoVertexIsolationBDT"
                                        }
                                      ],
                "SpdMult": 600,
                "Track_GhostProb": 0.5,
                "UseNoPIDsHadrons": True
                },
    "STREAMS": [ "Leptonic" ],
    "WGs": [ "RD" ]
    }

#################################


#stripping version
stripping='stripping21r0p1'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
#config  = strippingConfiguration(stripping)
config = {'B2XMuMu' : B2XMuMu} # Take the config as defined above
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)


#
# Merge into one stream and run in filter mode
#
AllStreams = StrippingStream("B2XMuMu.Strip")

linesToAdd = []
for stream in streams:
    if 'Leptonic' in stream.name():
        for line in stream.lines:
            if 'B2XMuMu' in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so we filter events by stripping selected lines

#
# Configure the dst writers for the output
#
enablePacking = True
#enablePacking = False

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements
                                       )

#
# Configuration of SelDSTWriter
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#

enablePacking = True

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'                : stripDSTStreamConf(pack=enablePacking)
    }

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
 
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x39112101)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2012"
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

# Save info of rejection factor in the filtering
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]

from GaudiConf import IOHelper

# Use the local input data
#IOHelper().inputFiles([
#    '/eos/lhcb/user/b/bdey/filteredMC/00058833_00000018_7.AllStreams.dst'
#], clear=True)


