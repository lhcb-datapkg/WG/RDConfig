"""
Options of Stripping28r2p2 for MC 2016,
@author Lisa Fantini
@data 27-05-2024
with only the BhadronCompleteEvent stream
"""


from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"


from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive


# kill old strip report
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = ['/Event/AllStreams',
                         '/Event/Strip']


stripping='stripping28r2p2'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)


streams = []
streams.append( quickBuild('BhadronCompleteEvent') )


AllStreams = StrippingStream("BhadronCompleteEvent.Strip")

linesToAdd = []

for stream in streams:
    for line in stream.lines:
        if 'Majo' in line.name() and 'Prescale' not in line.name():
#        if 'Beauty2MajoELLLine' in line.name():
            #line._prescale = 1.0
            line._postscale = 1.0
            linesToAdd.append(line)

AllStreams.appendLines(linesToAdd)


sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = -1 )

AllStreams.sequence().IgnoreFilterPassed = False


from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements
                                       )

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }



dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x421122922)

from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
#DaVinci().UseTrigRawEvent=True
DaVinci().EventPreFilters += [eventNodeKiller] 

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

#Retention rate local tests
#from Configurables import DumpFSR, DaVinci
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]


