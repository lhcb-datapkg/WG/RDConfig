"""
Stripping Filtering file for B2XMuMu S28r2 line, DST output, no PID
@author Maik Becker
@date   2020-09-02
"""

# Stripping version
stripping = 'stripping28r2'

# Use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Disable the cache in Tr/TrackExtrapolators
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

# Fix for TrackEff lines
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs", 4.2)

#############################################################################
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
AllStreams = StrippingStream("B2XMuMu_NoPID.Strip")

# Add B2XMuMu line without PID
from StrippingArchive.Utils import lineBuilderAndConf
my_builder, my_config = lineBuilderAndConf(stripping, 'B2XMuMu')
my_config['Muon_IsMuon'] = False
my_config['MuonPID'] = -1e30

AllStreams.appendLines(my_builder('B2XMuMuNoPID', my_config).lines())

prefix = 'Strip'
sc = StrippingConf(
    Streams = [AllStreams],
    MaxCandidates = 2000,
    TESPrefix = prefix
)

AllStreams.sequence().IgnoreFilterPassed = False

#############################################################################
# Configuration of SelDSTWriter
#
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTElements,
                                      stripDSTStreamConf)

SelDSTWriterElements = {
    'default': stripDSTElements(stripPrefix=prefix)
}

SelDSTWriterConf = {
    'default': stripDSTStreamConf(stripPrefix=prefix)
}

dstWriter = SelDSTWriter(
    "MyDSTWriter",
    StreamConf = SelDSTWriterConf,
    MicroDSTElements = SelDSTWriterElements,
    OutputFileSuffix = 'RD',
    SelectionSequences = sc.activeStreams()
)

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44105282)
# 0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"]
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

#############################################################################
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([stck])
DaVinci().appendToMainSequence([dstWriter.sequence()])
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60

## Save info of rejection factor in the filtering
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [DumpFSR()]
