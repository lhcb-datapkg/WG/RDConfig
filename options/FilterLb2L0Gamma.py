"""
MC-matching/reconstruction filtering for Hlt2 filtering requests
This script selects only those particle with an associated MCParticle.
This ensures the reconstructibility of the Lb2L0Gamma candidates.
@author Luis Miguel Garcia Martin
@date   2020-10-13
"""

########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, MergedSelection

#from CommonMCParticles import StandardMCLambda0, StandardMCPhotons, StandardMCKaons


def FilterBasic(name, code, loc):
    _Filter = FilterDesktop(
        '{name}_Filter'.format(name=name),
        Code=code,
        Preambulo=[
            "from LoKiPhysMC.decorators import *",
            "from PartProp.Nodes import CC"
        ])

    _input = DataOnDemand(Location='Phys/{loc}/Particles'.format(loc=loc))

    return Selection(
        '{name}_Sel'.format(name=name),
        Algorithm=_Filter,
        RequiredSelections=[_input])


###############################

_p_L = FilterBasic('MCProton_L', "(mcMatch('[p+]cc'))", 'StdNoPIDsProtons')
_p_D = FilterBasic('MCProton_D', "(mcMatch('[p+]cc'))", 'StdNoPIDsDownProtons')
_pi_L = FilterBasic('MCPion_L', "(mcMatch('[pi+]cc'))", 'StdNoPIDsPions')
_pi_D = FilterBasic('MCPion_D', "(mcMatch('[pi+]cc'))", 'StdNoPIDsDownPions')
_gamma = FilterBasic('MCPhoton', "(mcMatch('gamma'))", 'StdLoosePhotons')

_L02PPi = CombineParticles(
    "L0ToPPi",
    DecayDescriptor="[Lambda0 -> p+ pi-]cc",
    MotherCut="(mcMatch('[Lambda0 ==> p+ pi-]CC'))",
    Preambulo=[
        "from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"
    ])

_l0_LL = Selection(
    "Lambda_LL", Algorithm=_L02PPi, RequiredSelections=[_p_L, _pi_L])

_l0_DD = Selection(
    "Lambda_DD", Algorithm=_L02PPi, RequiredSelections=[_p_D, _pi_D])

_l0 = MergedSelection('Lambda', RequiredSelections=[_l0_LL, _l0_DD])

###############################

matchLb2L0gamma = "(mcMatch('[Lambda_b0  ==> Lambda0 gamma]CC'))"

_lb2L0gamma = CombineParticles("lb2L0gamma")
_lb2L0gamma.DecayDescriptor = "[Lambda_b0 -> Lambda0 gamma]cc"
_lb2L0gamma.DaughtersCuts = {"gamma": "PT>1.5*GeV"}
_lb2L0gamma.ParticleCombiners = {'': 'ParticleAdder'}
_lb2L0gamma.MotherCut = matchLb2L0gamma
_lb2L0gamma.Preambulo = [
    "from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"
]

SelLb2L0gamma = Selection(
    "SelLb2L0Gamma", Algorithm=_lb2L0gamma, RequiredSelections=[_l0, _gamma])

SeqLb2L0gamma = SelectionSequence('MCFilter', TopSelection=SelLb2L0gamma)

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter, stripDSTStreamConf,
                                      stripDSTElements)
SelDSTWriterElements = {'default': stripDSTElements(pack=enablePacking)}
SelDSTWriterConf = {'default': stripDSTStreamConf(fileExtension=dstExtension)}
dstWriter = SelDSTWriter(
    "MyDSTWriter",
    StreamConf=SelDSTWriterConf,
    MicroDSTElements=SelDSTWriterElements,
    OutputFileSuffix='SelLb2L0Gamma',
    SelectionSequences=[SeqLb2L0gamma],
)
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
#DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1  # Number of events
DaVinci().appendToMainSequence(
    [SeqLb2L0gamma.sequence(), dstWriter.sequence()])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60
