"""
Stripping Filtering file for B2MuMuMuMu S21 line, DST output
@author Alexander Battig
@date   2020-04-09
"""

#stripping version
stripping='stripping21'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
#from Configurables import TrackStateProvider
#TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database

B24mu = {
  'NAME'       : 'B2MuMuMuMu',
  'CONFIG'     : {
        'B2MuMuMuMuLinePrescale'    : 1,
        'B2MuMuMuMuLinePostscale'   : 1,
        'D2MuMuMuMuLinePrescale'    : 1,
        'D2MuMuMuMuLinePostscale'   : 1,
        'B2TwoDetachedDimuonLinePrescale'  : 1,
        'B2TwoDetachedDimuonLinePostscale' : 1,
        'B2JpsiKmumuLinePrescale'  : 1,
        'B2JpsiKmumuLinePostscale' : 1,
        'B2JpsiPhimumuLinePrescale'  : 1,
        'B2JpsiPhimumuLinePostscale' : 1,
        'B2DetachedDimuonAndJpsiLinePrescale' : 1,
        'B2DetachedDimuonAndJpsiLinePostscale': 1,
        'DetachedDiMuons': {
            'AMAXDOCA_MAX'  : '0.5*mm',
            'ASUMPT_MIN'    : '1000*MeV',
            'VCHI2DOF_MAX'  : 16,
            'BPVVDCHI2_MIN' : 16,
            },
        'B2DetachedDiMuons': {
            'SUMPT_MIN'        : '2000*MeV',
            'VCHI2DOF_MAX'     : 6,
            'BPVIPCHI2_MAX'    : 16,
            'BPVVDCHI2_MIN'    : 50,
            'BPVDIRA_MIN'      : 0.0,
            'MASS_MIN'         : {'B':'4600*MeV'},
            'MASS_MAX'         : {'B':'6000*MeV'}
            },
        'B2DetachedDiMuonsAndJpsi': {
            'SUMPT_MIN'        : '2000*MeV',
            'VCHI2DOF_MAX'     : 6,
            'BPVIPCHI2_MAX'    : 16,
            'BPVVDCHI2_MIN'    : 50,
            'BPVDIRA_MIN'      : 0.0,
            'MASS_MIN'         : {'B':'4600*MeV'},
            'MASS_MAX'         : {'B':'7000*MeV'}
            }
            },
  'BUILDERTYPE' : 'B2MuMuMuMuLinesConf',
  'WGs'     : 'RD',
  'STREAMS' : ['Bhadron'],
}

config = {"B2MuMuMuMu" : B24mu}
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)


#
# Merge into one stream and run in filter mode
#
AllStreams = StrippingStream("Bhadron.Strip")

linesToAdd = []
for stream in streams:
    if 'Bhadron' in stream.name():
        for line in stream.lines:
            if 'B2MuMuMuMuB24Mu' in line.name():
                print("#############################################################")
                print("#############################################################")
                print("Found my line!")
                print("#############################################################")
                print("#############################################################")

                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so we filter events by stripping selected lines

#
# Configure the dst writers for the output
#
enablePacking = True
#enablePacking = False

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements
                                       )


#############################################################################
#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements(stripPrefix='Strip')
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf(stripPrefix='Strip')
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='RD',
                          SelectionSequences = sc.activeStreams()
                          )



# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36152100)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().DataType = "2012"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60


