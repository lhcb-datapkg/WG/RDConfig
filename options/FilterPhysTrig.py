######################################################################
# 3) Filters
#
#
#
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    L0DU_Code  = " L0_DECISION_PHYSICS "
    , HLT_Code   = "HLT_PASS_RE('Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision') & ( HLT_PASS_RE('Hlt2(?!Forward)(?!DebugEvent)(?!Express)(?!Lumi)(?!Transparent)(?!PassThrough).*Decision') )" 
    )


from Configurables import DaVinci
DaVinci().EventPreFilters = trigfltrs.filters('PhysTrigFilters')


