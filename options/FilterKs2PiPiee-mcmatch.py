"""
MCMatch Filtering file for Ks->pipiee
@author Carla Marin
@date   2016-09-28
"""

from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPions, StandardMCElectrons

#Truth matched commonparticles:
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')
_elecs = DataOnDemand(Location='Phys/StdMCElectrons/Particles')

#
# MC matching
#

matchKs2pipiee   = "mcMatch('KS0 ==> pi+ pi- e+ e-')"

_Ks2pipiee = CombineParticles("Ks2pipiee")
_Ks2pipiee.DecayDescriptor = "KS0 -> pi+ pi- e+ e-"

_Ks2pipiee.MotherCut = matchKs2pipiee
_Ks2pipiee.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelKs2pipiee = Selection( "SelKs2pipiee",
                        Algorithm = _Ks2pipiee,
                        RequiredSelections=[_pions, _elecs])

SeqKs2pipiee = SelectionSequence('MCFilter', TopSelection = SelKs2pipiee) 

#
# Write DST
#
enablePacking = True
dstExtension = "." + DaVinci().InputType.lower()

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default' : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default' : stripDSTStreamConf(pack=enablePacking,
                                   fileExtension=dstExtension,
                                   selectiveRawEvent=False)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='MCMatch',
                          SelectionSequences = [ SeqKs2pipiee ],
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqKs2pipiee.sequence(),
                                 dstWriter.sequence() ])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
