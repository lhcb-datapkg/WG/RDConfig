"""
Stripping34 filtered production
Streams: 'Leptonic', 'Radiative'
@author David Gerick
@date   2019-01-31
"""

#use CommonParticlesArchive
stripping='stripping34'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = []
streams.append( buildStream(stripping=config, streamName='Leptonic', archive=archive) )
streams.append( buildStream(stripping=config, streamName='Radiative', archive=archive) )

AllStreams = StrippingStream("LeptonicRadiative.Strip")

for stream in streams:
    for line in stream.lines:
        if not 'Tau23MuDs23PiLine' in line.name():
            line._prescale = 1.0
    AllStreams.appendLines(stream.lines)

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip' )

AllStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements)

SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,
                                                    isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking,
                                                      selectiveRawEvent=False,
                                                      isMC=True)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44703400)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

#######################################################################
# comment this part out
#DaVinci().DataType = "2016"
# Save info of rejection factor in the filtering
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]

#from GaudiConf import IOHelper

# Use the local input data
#IOHelper().inputFiles([
#    '/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00063182/0000/00063182_00000236_7.AllStreams.dst'], clear=True)



