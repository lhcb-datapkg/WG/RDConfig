###################################################################
# MinBias trigger filtering on Topo
# Use: EventType 30000040, DecFile: minbias=Biased5TrkPt600MeV.dec
# Author: Marcin Kucharczyk, 16-04-2012
###################################################################
#
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT_Code = "HLT_PASS_RE('Hlt2.*Topo.*Decision')" 
    )

from Configurables import DaVinci
DaVinci().EventPreFilters = trigfltrs.filters('topoFilters')


