"""
MCMatch Filtering file for Ks->mumuee 
@author Adrian Casais Vidal, Xabier Cid Vidal, Carla Marin
@date   2018-09-26
"""

from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from Configurables import ChargedProtoParticleMaker,ChargedPP2MC,NoPIDsParticleMaker,EventSelector
from PhysSelPython.Wrappers import Selection, SelectionSequence

from StandardParticles import StdAllNoPIDsElectrons, StdNoPIDsUpElectrons, StdAllNoPIDsMuons
from CommonParticles.Utils import *

#
## Making VELO tracks available
#






myprotos = ChargedProtoParticleMaker("MyProtoParticles",
                                     Inputs = ["Rec/Track/Best"],
                                     Output = "Rec/ProtoP/MyProtoParticles")

protop_locations = [myprotos.Output]
charged = ChargedPP2MC("myprotos")
charged.InputData = protop_locations
myseq = GaudiSequencer("myseq")
myseq.Members +=[myprotos,charged]



                





algorithm_e =  NoPIDsParticleMaker ( 'StdNoPIDsVeloElectronsLocal',
                                   DecayDescriptor = 'Electron' ,
                                   Particle = 'electron',
                                   AddBremPhotonTo= [],
                                   Input = myprotos.Output)

algorithm_mu =  NoPIDsParticleMaker ( 'StdNoPIDsVeloMuonsLocal',
                                   DecayDescriptor = 'Muon' ,
                                   Particle = 'muon',
                                   AddBremPhotonTo= [],
                                   Input = myprotos.Output)
algorithm_mu_up =  NoPIDsParticleMaker ( 'StdNoPIDsUpMuonsLocal',
                                   DecayDescriptor = 'Muon' ,
                                   Particle = 'muon',
                                   AddBremPhotonTo= [],
                                   Input = myprotos.Output)




                                                                                                                   

selector_e = trackSelector ( algorithm_e,trackTypes = [ "Velo" ]  )
selector_mu = trackSelector ( algorithm_mu,trackTypes = [ "Velo" ]  )
selector_mu_up = trackSelector ( algorithm_mu_up,trackTypes = [ "Upstream" ]  )
locations_e = updateDoD ( algorithm_e )
locations_mu = updateDoD ( algorithm_mu )
locations_mu_up = updateDoD ( algorithm_mu_up )



from PhysSelPython.Wrappers import AutomaticData

StdNoPIDsVeloElectrons = AutomaticData(Location = "Phys/StdNoPIDsVeloElectronsLocal/Particles")
StdNoPIDsVeloMuons = AutomaticData(Location = "Phys/StdNoPIDsVeloMuonsLocal/Particles")
StdNoPIDsUpMuons = AutomaticData(Location = "Phys/StdNoPIDsUpMuonsLocal/Particles")

_sels_e = {"Long":StdAllNoPIDsElectrons,"Velo":StdNoPIDsVeloElectrons,"Up":StdNoPIDsUpElectrons}
_sels_mu = {"Long":StdAllNoPIDsMuons,"Velo":StdNoPIDsVeloMuons,"Up":StdNoPIDsUpMuons}
MatchedElectrons={}
MatchedMuons={}

MatchedElectrons_seq={}
MatchedMuons_seq={}
locations = {}


#
## Matched electrons locations
#

for key in ["Long","Velo","Up"]:
    if key == "Velo":
	    _loc = "'/Event/Relations/Rec/ProtoP/MyProtoParticles'"
	    _code = "(mcMatch('[KS0 => mu+  ^e-]CC', %s ))"% (_loc)
    if key == "Up":
	    _loc = "'Relations/Rec/ProtoP/Charged'"
	    _code = "(mcMatch('[KS0 => mu+  ^e-]CC', %s ))"% (_loc)
    if key == "Long":
	    _code = "(mcMatch('[KS0 => mu+  ^e-]CC'))"
    
    preambulo =[ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ]
    _Filter = FilterDesktop(Preambulo = preambulo,Code = _code)
    
    MatchedElectrons[key]  = Selection("Matched"+key+"Electrons",Algorithm = _Filter, RequiredSelections = [_sels_e[key]]   )
				       
    MatchedElectrons_seq[key] = SelectionSequence("Matched"+key+"ElectronsSequence", TopSelection=MatchedElectrons[key])

#
## Matched muons locations
#

for key in ["Long","Velo","Up"]:
    if key == "Velo":
	    _loc = "'/Event/Relations/Rec/ProtoP/MyProtoParticles'"
	    _code = "(mcMatch('[KS0 => ^mu+  e-]CC', %s ))"% (_loc)
    if key == "Up":
	    _loc = "'/Event/Relations/Rec/ProtoP/MyProtoParticles'"
	    _code = "(mcMatch('[KS0 => ^mu+  e-]CC', %s ))"% (_loc)
    if key == "Long":
	    _code = "(mcMatch('[KS0 => ^mu+  e-]CC'))"
    
    preambulo =[ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ]
    _Filter = FilterDesktop(Preambulo = preambulo,Code = _code)
    
    MatchedMuons[key]  = Selection("Matched"+key+"Muons",Algorithm = _Filter, RequiredSelections = [_sels_mu[key]]   )
    MatchedMuons_seq[key] = SelectionSequence("Matched"+key+"MuonsSequence", TopSelection=MatchedMuons[key])



    
#
## Setting up combinations
#

combs = {"LL":"( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 1 ) & ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 1 )",
         "UU":"( ANUM( ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 1 ) & ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == 'mu-' ) ) == 1 )",
         
         "LU":"( ANUM( ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 1 ) & ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 1 )",
         "LV":"( ANUM( ( TRTYPE == 1 ) &  ( ABSID == 'e-' ) ) == 1 ) & ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 1 )",
         "UV":"( ANUM( ( TRTYPE == 1 ) &  ( ABSID == 'e-' ) ) == 1 ) & ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == 'mu-' ) ) == 1 )"
	 }



selsKsemu={}
seqsksemu={}
Ksemu={}
for name in combs:
    Ksemu[name] = CombineParticles("TrackSel"+name+"_Ksemu")

    if "V" in name:
        Ksemu[name].DecayDescriptors = ["KS0 -> mu+ e-","KS0 -> mu- e+","KS0 -> mu+ e+","KS0 -> mu- e-"]
	
    else:
        Ksemu[name].DecayDescriptors = ["KS0 -> mu+ e-","KS0 -> mu- e+"]
    Ksemu[name].Preambulo=["from LoKiPhysMC.decorators import *",
                               "from LoKiPhysMC.functions import mcMatch"]
     

    
    Ksemu[name].CombinationCut = combs[name]
    Ksemu[name].MotherCut = "ALL"
    


    Ksemu[name].Inputs =[]

    if name=="UV":
	    Ksemu[name].Inputs+=['Phys/MatchedUpMuons/Particles','Phys/MatchedVeloElectrons/Particles']
	    selsKsemu[name]= Selection("SelKsemu_"+name,
			      Algorithm = Ksemu[name],
                              
			      RequiredSelections=[
						  AutomaticData(Location = 'Phys/MatchedUpMuons/Particles'),
    			       			  
    			       			  AutomaticData(Location = 'Phys/MatchedVeloElectrons/Particles')])
	    
    if name == "LV":
	    Ksemu[name].Inputs+=['Phys/MatchedVeloElectrons/Particles','Phys/MatchedLongMuons/Particles']
	    selsKsemu[name]= Selection("SelKsemu_"+name,
			      Algorithm = Ksemu[name],
                              
			      RequiredSelections= [
						  AutomaticData(Location = 'Phys/MatchedLongMuons/Particles'),
    			       			  AutomaticData(Location ='Phys/MatchedVeloElectrons/Particles')
    			       			  ])

    if name == "LL" :
	    Ksemu[name].Inputs+=['Phys/MatchedLongElectrons/Particles','Phys/MatchedLongMuons/Particles']
	    selsKsemu[name]= Selection("SelKsemu_"+name,
			      Algorithm = Ksemu[name],
                              
			      RequiredSelections=[AutomaticData(Location = 'Phys/MatchedLongElectrons/Particles'),
						  AutomaticData(Location = 'Phys/MatchedLongMuons/Particles')
    			       			  ])
    if name == "LU":
	    Ksemu[name].Inputs+=['Phys/MatchedUpElectrons/Particles','Phys/MatchedLongMuons/Particles']
	    selsKsemu[name]= Selection("SelKsemu_"+name,
			      Algorithm = Ksemu[name],
                              
			      RequiredSelections=[
						  AutomaticData(Location = 'Phys/MatchedLongMuons/Particles'),
						  AutomaticData(Location = 'Phys/MatchedUpElectrons/Particles')
    			       			  ])
    if name == "UU":
	    Ksemu[name].Inputs+=['Phys/MatchedUpElectrons/Particles','Phys/MatchedUpMuons/Particles']
	    selsKsemu[name]= Selection("SelKsemu_"+name,
			      Algorithm = Ksemu[name],
                              
			      RequiredSelections=[
						  AutomaticData(Location = 'Phys/MatchedUpMuons/Particles'),
						  AutomaticData(Location = 'Phys/MatchedUpElectrons/Particles')
    			       			  ])
	    
    seqsksemu[name]=SelectionSequence("MCFilter_"+name,TopSelection=selsKsemu[name])
                       

#
## Merging streams
#

from StrippingConf.Configuration import StrippingConf
from StrippingConf.StrippingLine import StrippingLine
from StrippingConf.StrippingStream import StrippingStream

strilines = {}
for key in selsKsemu:
	strilines[key] = StrippingLine("line"+key,
				       prescale  = 1,
				       postscale = 1,
				       selection = selsKsemu[key])
	
	
mystream = StrippingStream(name="KS0emu",Lines = strilines.values())

sc = StrippingConf( Streams = [ mystream ],
                    MaxCandidates = -1,
                    TESPrefix = 'Strip'
                    )

mystream.sequence().IgnoreFilterPassed = False # so we filter events by stripping selected lines


#
## Write DST
#

enablePacking = True
dstExtension = ".dst"

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default' : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default' : stripDSTStreamConf(pack=enablePacking,
                                   selectiveRawEvent=False)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='MCMatch',
			  SelectionSequences = sc.activeStreams()
                          )




# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41452811)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number




#
## DaVinci Configuration
#

DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence([myseq])
DaVinci().appendToMainSequence([ MatchedElectrons_seq[key] for key in MatchedElectrons_seq ])
DaVinci().appendToMainSequence([ MatchedMuons_seq[key] for key in MatchedMuons_seq ])
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([ stck ])
DaVinci().appendToMainSequence([ dstWriter.sequence() ])


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60


