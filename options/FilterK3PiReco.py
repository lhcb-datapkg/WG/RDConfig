########################################################################
'''
   Filtering for K-> pi pi pi decays
   Only reconstruction filtering, no stripping
   @author: Francesco Dettori
   @data: 6/10/2014

'''
from Gaudi.Configuration import *
from Configurables import DaVinci 
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPions

#Truth matched commonparticles: 
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')


#
# MC matching
#
matchK3Pi = "(mcMatch('[K+ ==> pi+ pi- pi+]CC'))"


## D0 -> mu+ mu-
_k3pi = CombineParticles( DecayDescriptor = "[K+ -> pi+ pi- pi+]cc"
                                , MotherCut = matchK3Pi
                                , Preambulo = [
                                        "from LoKiPhysMC.decorators import *",
                                        "from PartProp.Nodes import CC" ]
                              )

_selK3pi = Selection( "SelK3pi"
                            , Algorithm = _k3pi
                            , RequiredSelections = [_pions ] )


SeqK3pi = SelectionSequence('MCFilter', TopSelection = _selK3pi) 

#
# Write DST
#
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqK3pi ],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                       # Number of events
DaVinci().appendToMainSequence( [SeqK3pi.sequence(), dstWriter.sequence() ])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

