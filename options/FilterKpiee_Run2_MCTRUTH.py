from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 5 ],
				"CloneDistCut" : [5000, 9e+99 ] }


from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive


from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand
from Configurables import FilterDesktop
from Configurables import LoKiSvc

# truth matching standard particle

_stdPions = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
_pionFilter = FilterDesktop('pionFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
			    Code = "(mcMatch('[pi+]cc'))")

PionFilterSel = Selection(name = 'PionFilterSel',
			  Algorithm = _pionFilter,
			  RequiredSelections = [ _stdPions ])



_stdKaons = DataOnDemand(Location = "Phys/StdAllNoPIDsKaons/Particles")
_kaonFilter = FilterDesktop('kaonFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
			    Code = "(mcMatch('[K+]cc'))" )
KaonFilterSel = Selection(name = 'KaonFilterSel',
			  Algorithm = _kaonFilter,
			  RequiredSelections = [ _stdKaons])



_stdElectrons = DataOnDemand(Location = "Phys/StdAllNoPIDsElectrons/Particles")
_electronFilter = FilterDesktop('ElectronFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
			    Code = " (mcMatch('[e+]cc'))")

ElectronFilterSel = Selection(name = 'ElectronFilterSel',
			  Algorithm = _electronFilter,
			  RequiredSelections = [ _stdElectrons ])



_makeB0 = CombineParticles("make_B0",
			   DecayDescriptor = "[B0 -> K+ pi- e+ e- ]cc",
			   MotherCut = "mcMatch( '[ B0 ==> K+ pi- e+ e- ]CC' ) | mcMatch( '[ B0 ==> K- pi+ e- e+ ]CC' )" )#Is second part correct in original script?

selB0 = Selection("SelB0",
		  Algorithm = _makeB0,
		  RequiredSelections = [KaonFilterSel, PionFilterSel, ElectronFilterSel] )
seq = SelectionSequence("MCTrue", TopSelection=selB0 )
			   


#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
				      stripDSTStreamConf,
				      stripDSTElements
				      )

SelDSTWriterElements = {
	'default'               : stripDSTElements(pack=enablePacking)
	     }

SelDSTWriterConf = {
	'default'               : stripDSTStreamConf(pack=enablePacking)
	     }

dstWriter = SelDSTWriter( "MyDSTWriter",
			                             StreamConf = SelDSTWriterConf,
			                             MicroDSTElements = SelDSTWriterElements,
			                             OutputFileSuffix ='Filtered_Kpiee_Run2',
			                             SelectionSequences = [seq]
			                             )

from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ seq.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

