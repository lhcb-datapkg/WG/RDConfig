"""
Stripping34r0p2 filtered production for StrippingB2XTauTauMuonic_Bs2Phi_Line
No PID requirements on hadrons or muons.
Outputs to DST.

@author H. Tilquin
@date   2022-01-25
"""

# Stripping version
stripping='stripping34r0p2'

# Use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Disable the cache in Tr/TrackExtrapolators
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

# Fix for TrackEff lines
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs", 4.2)

# Build the streams and stripping object
from StrippingConf.Configuration import StrippingConf, StrippingStream
MyStream = StrippingStream("PhiTauTau.Strip")

# Get the configuration dictionary
from StrippingSelections.StrippingRD.StrippingB2XTauTauMuonic import default_config as config
myConfig = config['CONFIG']
myConfig['MuonPID'] = -1001
myConfig['UseNoPIDsHadrons'] = True

from StrippingSelections.StrippingRD.StrippingB2XTauTauMuonic import B2XTauTauMuonicConf as builder
line_builder = builder('B2XTauTauMuonic', myConfig)

for line in line_builder.lines():
    if line.name() == 'StrippingB2XTauTauMuonic_Bs2Phi_Line':
        line.checkPV = False
        MyStream.appendLines([line])


sc = StrippingConf( Streams=[MyStream],
                    MaxCandidates=2000,
                    TESPrefix='Strip' )


MyStream.sequence().IgnoreFilterPassed = False

# Configuration of SelDSTWriter
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default': stripDSTElements(stripPrefix='Strip')
    }

SelDSTWriterConf = {
    'default' : stripDSTStreamConf(stripPrefix='Strip')
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf=SelDSTWriterConf,
                          MicroDSTElements=SelDSTWriterElements,
                          OutputFileSuffix='RD',
                          SelectionSequences=sc.activeStreams()
                          )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44113402)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

# DaVinci Configuration
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().DataType = "2018"

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
