"""
Stripping Filtering file for Lb->(N* -> p \pi) \gamma
@author Alejandro Alfonso
@date   2018-09-27
"""

#stripping version
stripping='stripping21'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
#from Configurables import TrackStateProvider
#TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
#from Configurables import DecodeRawEvent
#DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)


#
# Merge into one stream and run in filter mode
#
AllStreams = StrippingStream("Lb2NstGamma.Strip")

linesToAdd = []
for stream in streams:
    if 'Leptonic' in stream.name():
        for line in stream.lines:
            if ('B2XGamma2pi_Line' in line.name()) or ('B2XGammapi_Ks0_Line' in line.name()):
                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so we filter events by stripping selected lines

#
# Configure the dst writers for the output
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripMicroDSTStreamConf,
                                       stripMicroDSTElements
                                       )

#
# Configure Micro DST
#
mdstElements   = stripMicroDSTElements(pack=enablePacking, isMC=True)
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking,
                                         selectiveRawEvent=True,
                                         isMC=True)
#
# Configure SelMicroDSTWriter
#

SelDSTWriterElements = {
    'default'               : mdstElements,
    }

SelDSTWriterConf = {
    'default'                : mdstStreamConf,
    }

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

#Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36132100)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().DataType = '2012'

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
