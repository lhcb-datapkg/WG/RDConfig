"""
Stripping Filtering file for the Bd2Kstee decays, 
i.e. without PID defined but with MC matched in each track  

@author Rafael Silva Coutinho, Fabrice Desse 
@date   2019-08-29
"""
from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Configuration of my costumized stripping line
#

daughter_locations = {
    # OPPOSITE SIGN
    # 3-body
    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC" : "{0}H",
    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC" : "{0}H3",
    # 5-body
    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC" : "{0}H3",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}HH",
    # 4-body with a strange particle in the final state
    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body with two pions in the final state
    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 4-body with proton and pion in the final state
    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC" : "{0}LL",

    # 7-body (quasi 4-body)
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC" : "{0}L21",
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC" : "{0}L22",
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC" : "{0}L23",
    
    # SAME SIGN
    # 3-body
    "[Beauty -> ^X+  (X+ ->  l+  l+)]CC" : "{0}H",
    "[Beauty ->  X+  (X+ -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  X+  (X+ ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty ->  X+ ^(X+ ->  l+  l+)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> (X+ -> ^X+  X+  X-) (X+ ->  l+  l+)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+ ^X+  X-) (X+ ->  l+  l+)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  X+ ^X-) (X+ ->  l+  l+)]CC" : "{0}H3",
    # 5-body
    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X+ ->  l+  l+)]CC" : "{0}H1",
    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X+ ->  l+  l+)]CC" : "{0}H2",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X+ ->  l+  l+)]CC" : "{0}H3",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X+ ->  l+  l+)]CC" : "{0}HH",
    # 4-body with a strange particle in the final state
    "[Beauty ->  (X0 -> ^Xs  X-)  (X+ ->  l+  l+)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X+ ->  l+  l+)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X+ -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X+ ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X+ ->  l+  l+)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X+ ->  l+  l+)]CC" : "{0}LL",
    # 4-body with two pions in the final state
    "[Beauty ->  (X0 -> ^pi+  pi-)  (X+ ->  l+  l+)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X+ ->  l+  l+)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  pi+  pi-)  (X+ ->  l+  l+)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  pi+  pi-) ^(X+ ->  l+  l+)]CC" : "{0}LL",
    # 4-body with proton and pion in the final state
    "[Beauty ->  (X0 -> ^p+  pi-)  (X+ ->  l+  l+)]CC" : "{0}H1",
    "[Beauty ->  (X0 ->  p+ ^pi-)  (X+ ->  l+  l+)]CC" : "{0}H2",
    "[Beauty ->  (X0 ->  p+  pi-)  (X+ -> ^l+  l+)]CC" : "{0}L1",
    "[Beauty ->  (X0 ->  p+  pi-)  (X+ ->  l+ ^l+)]CC" : "{0}L2",
    "[Beauty -> ^(X0 ->  p+  pi-)  (X+ ->  l+  l+)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  p+  pi-) ^(X+ ->  l+  l+)]CC" : "{0}LL",
    # 7-body (quasi 4-body)
    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ -> ^X+  X-  X+))]CC" : "{0}L21",
    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+ ^X-  X+))]CC" : "{0}L22",
    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+  X- ^X+))]CC" : "{0}L23"
}

daughter_vtx_locations = {
    # OPPOSITE SIGN
    # 3-body
    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> ^(X+ -> X+  X+  X-) (X0 ->  l+  l-)]CC" : "{0}H",
    # 5-body
    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}H",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC" : "{0}HH",
    # 4-body
    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC" : "{0}LL",
    # 7-body (quasi 4-body)
    "[Beauty ->  X  (X0 ->  l+  ^(l- -> X-  X-  X+))]CC" : "{0}L",

    # SAME SIGN
    # 3-body
    "[Beauty ->  X+ ^(X+ ->  l+  l+)]CC" : "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> ^(X+ -> X+  X+  X-) (X+ ->  l+  l+)]CC" : "{0}H",
    # 5-body
    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X+ ->  l+  l+)]CC" : "{0}H",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X+ ->  l+  l+)]CC" : "{0}HH",
    # 4-body
    "[Beauty -> ^(X0 ->  X+  X-)  (X+ ->  l+  l+)]CC" : "{0}HH",
    "[Beauty ->  (X0 ->  X+  X-) ^(X+ ->  l+  l+)]CC" : "{0}LL",
    # 7-body (quasi 4-body)
    "[Beauty ->  X  (X+ ->  l+  ^(l+ -> X-  X-  X+))]CC" : "{0}L",
}

config = {                       
        'BFlightCHI2'            : 100   
        , 'BDIRA'                : 0.9995 
        , 'BIPCHI2'              : 25   
        , 'BVertexCHI2'          : 9    
        , 'DiLeptonPT'           : 0    
        , 'DiLeptonFDCHI2'       : 16   
        , 'DiLeptonIPCHI2'       : 0    
        , 'LeptonIPCHI2'         : 9   
        , 'LeptonPT'             : 300  
        , 'TauPT'                : 0
        , 'TauVCHI2DOF'          : 150
        , 'KaonIPCHI2'           : 9
        , 'KaonPT'               : 400
        , 'KstarPVertexCHI2'     : 25
        , 'KstarPMassWindow'     : 300
        , 'KstarPADOCACHI2'      : 30
        , 'DiHadronMass'         : 2600
        , 'UpperMass'            : 5500
        , 'BMassWindow'          : 1500
        , 'BMassWindowTau'       : 5000
        , 'PIDe'                 : 0
        , 'Trk_Chi2'             : 3
        , 'Trk_GhostProb'        : 0.4
        , 'K1_MassWindow_Lo'     : 0
        , 'K1_MassWindow_Hi'     : 6000
        , 'K1_VtxChi2'           : 12
        , 'K1_SumPTHad'          : 800
        , 'K1_SumIPChi2Had'      : 48.0  
        , 'RelatedInfoTools'     : [
            {'Type'              : 'RelInfoVertexIsolation',
             'Location'          : 'VertexIsoInfo',
             'IgnoreUnmatchedDescriptors': True, 
             'DaughterLocations' : {key: val.format('VertexIsoInfo') for key, val in daughter_vtx_locations.items()}},
            {'Type'              : 'RelInfoVertexIsolationBDT',
             'Location'          : 'VertexIsoBDTInfo',
             'IgnoreUnmatchedDescriptors': True, 
             'DaughterLocations' : {key: val.format('VertexIsoBDTInfo') for key, val in daughter_vtx_locations.items()}},
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 0.5,
             'IgnoreUnmatchedDescriptors': True, 
             'Location'          : 'TrackIsoInfo05',
             'DaughterLocations' : {key: val.format('TrackIsoInfo') for key, val in daughter_locations.items()}},
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'          : 0.5,
             'IgnoreUnmatchedDescriptors': True, 
             'Location'          : 'ConeIsoInfo05',
             'DaughterLocations' : {key: val.format('ConeIsoInfo') for key, val in daughter_locations.items()}},
            {'Type'              : 'RelInfoBs2MuMuTrackIsolations',
             'IgnoreUnmatchedDescriptors': True,
             'Location'          : 'TrackIsoBs2MMInfo',
             'DaughterLocations' : {key: val.format('TrackIsoBs2MMInfo') for key, val in daughter_locations.items()}},
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 1.0,
             'Location'          : 'TrackIsoInfo10'
            },
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 1.5,
             'Location'          : 'TrackIsoInfo15'
            },
            {'Type'              : 'RelInfoConeVariables',
             'ConeAngle'         : 2.0,
             'Location'          : 'TrackIsoInfo20'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'          : 1.0,
             'Location'          : 'ConeIsoInfo10'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'          : 1.5,
             'Location'          : 'ConeIsoInfo15'
            },
            {'Type'              : 'RelInfoConeIsolation',
             'ConeSize'          : 2.0,
             'Location'          : 'ConeIsoInfo20'
            }
            ],
        }
    

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import *
from CommonParticles.Utils import *
    

class Bu2LLK_NoPID_LineBuilder(LineBuilder) :
        
    # now just define keys. Default values are fixed later
    __configuration_keys__ = ( 
        'BFlightCHI2'        
        , 'BDIRA'             
        , 'BIPCHI2'           
        , 'BVertexCHI2'       
        , 'DiLeptonPT'        
        , 'DiLeptonFDCHI2'     
        , 'DiLeptonIPCHI2'     
        , 'LeptonIPCHI2'       
        , 'LeptonPT'          
        , 'TauPT' 
        , 'TauVCHI2DOF'
        , 'KaonIPCHI2'        
        , 'KaonPT'
        , 'KstarPVertexCHI2'
        , 'KstarPMassWindow'
        , 'KstarPADOCACHI2'
        , 'DiHadronMass'               
        , 'UpperMass'
        , 'BMassWindow'
        , 'BMassWindowTau'
        , 'PIDe' 
        , 'Trk_Chi2'
        , 'Trk_GhostProb'
        , 'K1_MassWindow_Lo'
        , 'K1_MassWindow_Hi'
        , 'K1_VtxChi2'
        , 'K1_SumPTHad'
        , 'K1_SumIPChi2Had'
        , 'RelatedInfoTools'
      )
    
    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self._name = name

        eeXLine_name   = name + "_ee"

        from StandardParticles import StdAllNoPIDsPions as Pions
        from StandardParticles import StdAllNoPIDsKaons as Kaons
        
        _pionFilter = FilterDesktop( Code = "(mcMatch('[pi+]cc'))",  
                                     Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"])
 
        PionFilterSel = Selection(name = 'PionFilterSel',
			  Algorithm = _pionFilter,
			  RequiredSelections = [ Pions ])

        _kaonFilter = FilterDesktop( Code = "(mcMatch('[K+]cc'))", 
                                     Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"])

        KaonFilterSel = Selection(name = 'KaonFilterSel',
			  Algorithm = _kaonFilter,
			  RequiredSelections = [ Kaons ])
        
        # Kstars
        CombKstars = CombineParticles()
        CombKstars.DecayDescriptor = "[K*(892)0 -> K+ pi-]cc"
        CombKstars.CombinationCut = "(APT > 500.*MeV) & (ADAMASS('K*(892)0') < 300.*MeV) & (ADOCACHI2CUT(30, ''))"
        CombKstars.MotherCut = "(VFASPF(VCHI2) < 25.) & (mcMatch( '[K*(892)0 => K+ pi-]CC' ))"
        CombKstars.Preambulo = ["from LoKiPhysMC.decorators import *", 
                                "from LoKiPhysMC.functions import mcMatch"]
        Kstars = Selection("SelNoPIDKstars", Algorithm = CombKstars, RequiredSelections = [ KaonFilterSel, PionFilterSel ] )
 
        # 1 : Make K, K*, K1
        SelKaons  = self._filterHadron( name   = "KaonsFor" + self._name,
                                        sel    = KaonFilterSel,
                                        params = config )

        SelKstars = self._filterHadron( name   = "KstarsFor" + self._name,
                                        sel    = Kstars,
                                        params = config )

        # 2 : Make Dileptons
        from StandardParticles import StdAllNoPIDsElectrons as Electrons

        _electronFilter = FilterDesktop( Code = "(mcMatch('[e+]cc'))", 
                                     Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"])

        ElectronFilterSel = Selection(name = 'ElectronFilterSel',
			  Algorithm = _electronFilter,
			  RequiredSelections = [ Electrons ])

        CombDiElectrons = CombineParticles()
        CombDiElectrons.DecayDescriptor = "J/psi(1S) -> e+ e-"
        CombDiElectrons.DaughtersCuts = { "e+" : "(PT>500*MeV)" }
        CombDiElectrons.CombinationCut = "(AM>30*MeV) & (ADOCACHI2CUT(30,''))"
        CombDiElectrons.MotherCut = "(VFASPF(VCHI2)<25)"

        DiElectrons = Selection("SelNoPIDDiElectrons", Algorithm = CombDiElectrons, RequiredSelections = [ ElectronFilterSel ] )

        # Now same container as above, but with DiElectronMaker
        from Configurables import DiElectronMaker, ProtoParticleCALOFilter
        from GaudiKernel.SystemOfUnits import *
        from CommonParticles.Utils import *

        MakeDiElectronsFromTracks = DiElectronMaker("MakeNoPIDDiElectronsFromTracks")
        MakeDiElectronsFromTracks.Particle = "J/psi(1S)"
        selector = trackSelector (MakeDiElectronsFromTracks , trackTypes = ["Long"])
        MakeDiElectronsFromTracks.addTool(ProtoParticleCALOFilter, name='Electron')
        MakeDiElectronsFromTracks.Electron.Selection = ["RequiresDet='CALO'"]
        MakeDiElectronsFromTracks.DiElectronMassMax = 1000000.*GeV #just to give a high limit. 
        MakeDiElectronsFromTracks.DiElectronMassMin = 0.*MeV 
        MakeDiElectronsFromTracks.DiElectronPtMin = 200.*MeV 
        DiElectronsFromTracks = Selection("SelNoPIDDiElectronsFromTracks", Algorithm = MakeDiElectronsFromTracks)

        SelDiElectron = self._filterDiLepton( "SelDiElectronFor" + self._name, 
                                              dilepton = DiElectrons,
                                              params   = config,
                                              idcut    = None )

        SelDiElectronFromTracks = self._filterDiLepton( "SelDiElectronFromTracksFor" + self._name, 
                                              dilepton = DiElectronsFromTracks,
                                              params   = config,
                                              idcut    = None )
        
        # 4 : Combine Particles
        SelB2eeX = self._makeB2LLX(eeXLine_name,
                                   dilepton = SelDiElectron,
                                   hadrons  = [SelKaons, SelKstars],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )

        SelB2eeXFromTracks = self._makeB2LLX(eeXLine_name + "2",
                                   dilepton = SelDiElectronFromTracks,
                                   hadrons  = [SelKaons, SelKstars],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV" % config )

        # 5 : Declare Lines
        SPDFilter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 600 )" ,
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb" ]
            }
        
        self.B2eeXLine = StrippingLine(eeXLine_name + "Line",
                                       prescale          = 1,
                                       postscale         = 1,
                                       selection         = SelB2eeX,
                                       RelatedInfoTools  = config['RelatedInfoTools'],
                                       FILTER            = SPDFilter, 
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False )

        self.B2eeXFromTracksLine = StrippingLine(eeXLine_name + "Line2",
                                        prescale          = 1,
                                        postscale         = 1,
                                        selection         = SelB2eeXFromTracks,
                                        RelatedInfoTools  = config['RelatedInfoTools'],
                                        FILTER            = SPDFilter, 
                                        RequiredRawEvents = [],
                                        MDSTFlag          = False )

        # 6 : Register Lines
        self.registerLine( self.B2eeXLine )
        self.registerLine( self.B2eeXFromTracksLine )

#####################################################
    def _filterHadron( self, name, sel, params ):
        """
        Filter for all hadronic final states
        """
        _Code = "(PT > %(KaonPT)s *MeV) & " \
                "(M < %(DiHadronMass)s*MeV) & " \
                "((ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) | " \
                "(NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))))" % params

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = [ sel ] )
#####################################################
    def _filterDiLepton( self, name, dilepton, params, idcut = None ) :
        """
        Handy interface for dilepton filter
        """

        _Code = "(ID=='J/psi(1S)') & "\
                "(PT > %(DiLeptonPT)s *MeV) & "\
                "(MM < %(UpperMass)s *MeV) & "\
                "(MINTREE(ABSID<14,PT) > %(LeptonPT)s *MeV) & "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "\
                "(VFASPF(VCHI2/VDOF) < 9) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "\
                "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params

        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut ) 

        _Filter = FilterDesktop( Code = _Code )
    
        return Selection(name, Algorithm = _Filter, RequiredSelections = [ dilepton ] )

####################################################
    def _makeB2LLX( self, name, dilepton, hadrons, params, masscut = "(ADAMASS('B+')< 1500 *MeV" ):
        """
        CombineParticles / Selection for the B 
        """

        _Decays = [ "[ B+ -> J/psi(1S) K+ ]cc",
                    "[ B0 -> J/psi(1S) K*(892)0 ]cc" ]
        
        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params
        
        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )
        
        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] ) 

#####################################################
#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping34'

AllStreams = StrippingStream("Bu2LLK_NoPID_MCMatch_BdKstee.Strip")
myBuilder = Bu2LLK_NoPID_LineBuilder('Bu2LLKNoPIDMCMatch', config)
AllStreams.appendLines( myBuilder.lines() )

prefix = 'Strip'
sc = StrippingConf( Streams = [ AllStreams ],
                    TESPrefix = prefix,
                    MaxCandidates = 2000 )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements, stripMicroDSTStreamConf, stripMicroDSTElements

#############################################################################
#
# Configuration of SelDSTWriter
#
#############################################################################

write_mdst = False

if write_mdst:
    SelDSTWriterElements = {'default': stripMicroDSTElements(pack=True, isMC=True)}
    SelDSTWriterConf = {'default': stripMicroDSTStreamConf(pack=True, isMC=True)}
else:
    SelDSTWriterElements = {'default': stripDSTElements(stripPrefix=prefix)}
    SelDSTWriterConf = {'default': stripDSTStreamConf(stripPrefix=prefix)}

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='RD',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44703400) 

test_script = False

if test_script :
    from Configurables import EventNodeKiller
    event_node_killer = EventNodeKiller('StripKiller')
    event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

#################################################################
#
# DaVinci Configuration
#
#################################################################
from Configurables import DaVinci
DaVinci().Simulation = True
if test_script :
    DaVinci().EvtMax = 1000
else :
    DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
if test_script :
    DaVinci().appendToMainSequence( [ event_node_killer ] )
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().DataType = "2018"
