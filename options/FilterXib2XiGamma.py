"""
MC-matching/reconstruction filtering for Hlt2 filtering requests
This script selects only those particle with an associated MCParticle.
This ensures the reconstructibility of the Xib2XiGamma candidates.
@author Luis Miguel Garcia Martin
@date   2020-10-13
"""

########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, MergedSelection


def FilterBasic(name, code, loc):
    _Filter = FilterDesktop(
        '{name}_Filter'.format(name=name),
        Code=code,
        Preambulo=[
            "from LoKiPhysMC.decorators import *",
            "from PartProp.Nodes import CC"
        ])

    _input = DataOnDemand(Location='Phys/{loc}/Particles'.format(loc=loc))

    return Selection(
        '{name}_Sel'.format(name=name),
        Algorithm=_Filter,
        RequiredSelections=[_input])


###############################

_p_L = FilterBasic('MCProton_L', "(mcMatch('[p+]cc'))", 'StdNoPIDsProtons')
_p_D = FilterBasic('MCProton_D', "(mcMatch('[p+]cc'))", 'StdNoPIDsDownProtons')
_pi_L = FilterBasic('MCPion_L', "(mcMatch('[pi+]cc'))", 'StdNoPIDsPions')
_pi_D = FilterBasic('MCPion_D', "(mcMatch('[pi+]cc'))", 'StdNoPIDsDownPions')
_gamma = FilterBasic('MCPhoton', "(mcMatch('gamma'))", 'StdLoosePhotons')

_L02PPi = CombineParticles(
    "L0ToPPi",
    DecayDescriptor="[Lambda0 -> p+ pi-]cc",
    MotherCut="(mcMatch('[Lambda0 ==> p+ pi-]CC'))",
    Preambulo=[
        "from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"
    ])

_l0_LL = Selection(
    "Lambda_LL", Algorithm=_L02PPi, RequiredSelections=[_p_L, _pi_L])

_l0_DD = Selection(
    "Lambda_DD", Algorithm=_L02PPi, RequiredSelections=[_p_D, _pi_D])
#
# Xi -> L0 pi
#
matchXi2L0pi = "(mcMatch('[Xi-  ==> Lambda0 pi-]CC'))"

_Xi2L0pi = CombineParticles("Xi2L0pi")
_Xi2L0pi.DecayDescriptor = "[Xi-  -> Lambda0 pi-]cc"
_Xi2L0pi.MotherCut = matchXi2L0pi
_Xi2L0pi.Preambulo = [
    "from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"
]

SelXi2L0pi_LLL = Selection(
    "SelXi2L0pi_LLL", Algorithm=_Xi2L0pi, RequiredSelections=[_l0_LL, _pi_L])

SelXi2L0pi_DDL = Selection(
    "SelXi2L0pi_DDL", Algorithm=_Xi2L0pi, RequiredSelections=[_l0_DD, _pi_L])

SelXi2L0pi_DDD = Selection(
    "SelXi2L0pi_DDD", Algorithm=_Xi2L0pi, RequiredSelections=[_l0_DD, _pi_D])

SelXi2L0pi = MergedSelection(
    'SelXi2L0pi',
    RequiredSelections=[SelXi2L0pi_LLL, SelXi2L0pi_DDL, SelXi2L0pi_DDD])

#
# Xib -> Xi Gamma
#

matchXib2Xigamma = "(mcMatch('[Xi_b-  ==> Xi- gamma]CC'))"

_Xib2Xigamma = CombineParticles("Xib2Xigamma")
_Xib2Xigamma.DecayDescriptor = "[Xi_b-  -> Xi- gamma]cc"
_Xib2Xigamma.DaughtersCuts = {"gamma": "PT>1.5*GeV"}
_Xib2Xigamma.ParticleCombiners = {'': 'ParticleAdder'}
_Xib2Xigamma.MotherCut = matchXib2Xigamma
_Xib2Xigamma.Preambulo = [
    "from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"
]

SelXib2Xigamma = Selection(
    "SelXib2XiGamma",
    Algorithm=_Xib2Xigamma,
    RequiredSelections=[SelXi2L0pi, _gamma])

SeqXib2Xigamma = SelectionSequence('MCFilter', TopSelection=SelXib2Xigamma)

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter, stripDSTStreamConf,
                                      stripDSTElements)
SelDSTWriterElements = {'default': stripDSTElements(pack=enablePacking)}
SelDSTWriterConf = {'default': stripDSTStreamConf(fileExtension=dstExtension)}
dstWriter = SelDSTWriter(
    "MyDSTWriter",
    StreamConf=SelDSTWriterConf,
    MicroDSTElements=SelDSTWriterElements,
    OutputFileSuffix='SelXib2XiGamma',
    SelectionSequences=[SeqXib2Xigamma],
)
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
#DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1  # Number of events
DaVinci().appendToMainSequence(
    [SeqXib2Xigamma.sequence(),
     dstWriter.sequence()])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60
