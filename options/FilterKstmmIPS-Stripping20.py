
#######################################################################
# 0) Smear the 'Best' track container
#
#
from Configurables import TrackSmeared, GaudiSequencer
TrackSmeared("TrackSmearing").InputLocation = "Rec/Track/Best"
TrackSmeared("TrackSmearing").OutputLocation = "Best"
TrackSmeared("TrackSmearing").smear = True
TrackSmeared("TrackSmearing").makePlots = True
TrackSmeared("TrackSmearing").OutputLevel = 3
TrackSmeared("TrackSmearing").smearBest = True
TrackSmearingSeq = GaudiSequencer("TrackSmearingSeq")
TrackSmearingSeq.Members = [ TrackSmeared("TrackSmearing") ]



#######################################################################
# 1)Run the stripping
#
#
from StrippingConf.Configuration import StrippingConf
from StrippingConf.StrippingStream import StrippingStream
from StrippingSelections.StrippingBd2KstarMuMu import StrippingBdToKstarMuMuConf
from StrippingSettings.Stripping20.LineConfigDictionaries_RDWG import Bd2KstarMuMu
BdToKstarMuMuConf = StrippingBdToKstarMuMuConf("BdToKstarMuMu", Bd2KstarMuMu['CONFIG'])
BdToKstarMuMuLines = BdToKstarMuMuConf.lines()
for line in BdToKstarMuMuLines:
     print "AS DEBUG", line
     print "AS DEBUG", line.name()
stripsel = BdToKstarMuMuLines[0]
stripsel.OutputLevel = 2
stripsel2 = BdToKstarMuMuLines[2]
stripsel2.OutputLevel = 2
##==============================================================
sc = StrippingConf( HDRLocation = "/Event/Strip/Phys/DecReports"  )
sc.OutputLevel = 2
stream = StrippingStream("Kstmm.StripTrig")
stream.appendLines([  stripsel , stripsel2  ] )
stream.OutputLevel = 2
sc.appendStream( stream )
#print "IGNOREFILTERPASSED", sc.sequence().IgnoreFilterPassed
########################################################################

######################################################################
# 3) Filters
#
#
#
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    L0DU_Code  = " L0_CHANNEL('Muon') "
    , HLT_Code   = " HLT_PASS_RE('Hlt1.*Track.*Decision') & ( HLT_PASS_RE('Hlt2.*Topo.*Decision') | HLT_PASS_RE('Hlt2SingleMuonDecision') | HLT_PASS_RE('Hlt2DiMuonDetachedDecision') )" 
   # , STRIP_Code = " HLT_PASS('StrippingBd2KstarMuMu_BdToKstarMuMuLineDecision') | HLT_PASS('StrippingBd2KstarMuMu_BdToKstarMuMuLowPLineDecision') "
    )


from Configurables import DaVinci
########################################################################
DaVinci().UserAlgorithms += [ TrackSmearingSeq ]
DaVinci().UserAlgorithms += [ sc.sequence() ]
#DaVinci().UserAlgorithms +=  trigfltrs.filters('KstmmFilters') 
DaVinci().EventPreFilters = trigfltrs.filters('KstmmFilters')


from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )
#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = { 'default' : stripDSTElements() }


SelDSTWriterConf = {'default': stripDSTStreamConf() }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Stripped',
                          SelectionSequences = sc.activeStreams()
                          )

DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().InputType     = 'DST'
DaVinci().DataType     = '2012'
DaVinci().Simulation   = True
DaVinci().Lumi = False
DaVinci().EvtMax       = -1
from Configurables import MessageSvc
MessageSvc().Format = "% F%80W%S%7W%R%T %0W%M"

