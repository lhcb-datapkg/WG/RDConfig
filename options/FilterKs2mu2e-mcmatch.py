"""
MCMatch Filtering file for Ks->mumuee 
@author Adrian Casais Vidal, Xabier Cid Vidal, Carla Marin
@date   2018-09-20
"""

from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from Configurables import ChargedProtoParticleMaker,ChargedPP2MC,NoPIDsParticleMaker,EventSelector
from PhysSelPython.Wrappers import Selection, SelectionSequence

from StandardParticles import StdAllNoPIDsElectrons, StdNoPIDsUpElectrons, StdAllNoPIDsMuons
from CommonParticles.Utils import *

#
## Making VELO tracks available
#


myprotos = ChargedProtoParticleMaker("MyProtoParticles",
                                     Inputs = ["Rec/Track/Best"],
                                     Output = "Rec/ProtoP/MyProtoParticles")

protop_locations = [myprotos.Output]
charged = ChargedPP2MC("myprotos")
charged.InputData = protop_locations
myseq = GaudiSequencer("myseq")
myseq.Members +=[myprotos,charged]


                


algorithm =  NoPIDsParticleMaker ( 'StdNoPIDsVeloElectronsLocal',
                                   DecayDescriptor = 'Electron' ,
                                   Particle = 'electron',
                                   AddBremPhotonTo= [],
                                   Input = myprotos.Output)


                                                                                                                   

selector = trackSelector ( algorithm,trackTypes = [ "Velo" ]  )
locations = updateDoD ( algorithm )

from PhysSelPython.Wrappers import AutomaticData

StdNoPIDsVeloElectrons = AutomaticData(Location = "Phys/StdNoPIDsVeloElectronsLocal/Particles")

_sels = {"Long":StdAllNoPIDsElectrons,"Velo":StdNoPIDsVeloElectrons,"Up":StdNoPIDsUpElectrons}
MatchedElectrons={}
MatchedElectrons_seq={}
locations = {}


#
## Matched electrons locations
#

for key in ["Long","Velo","Up"]:
    if key == "Velo":
	    _loc = "'/Event/Relations/Rec/ProtoP/MyProtoParticles'"
	    _code = "(mcMatch('KS0 => mu+ mu- ^e+ e-', %s )) | (mcMatch('KS0 => mu+ mu- e+ ^e-', %s ))"%(_loc,_loc)
    if key == "Up":
	    _loc = "'Relations/Rec/ProtoP/Charged'"
	    _code = "(mcMatch('KS0 => mu+ mu- ^e+ e-', %s )) | (mcMatch('KS0 => mu+ mu- e+ ^e-', %s ))"%(_loc,_loc)
    if key == "Long":
	    _code = "(mcMatch('KS0 => mu+ mu- ^e+ e-')) | (mcMatch('KS0 => mu+ mu- e+ ^e-'))"
    
    preambulo =[ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ]
    _Filter = FilterDesktop(Preambulo = preambulo,Code = _code)
    
    MatchedElectrons[key]  = Selection("Matched"+key+"Electrons",Algorithm = _Filter, RequiredSelections = [_sels[key]]   )
				       
    MatchedElectrons_seq[key] = SelectionSequence("Matched"+key+"ElectronsSequence", TopSelection=MatchedElectrons[key])

#
## Matched muons locations
#

_code = "(mcMatch('KS0 => ^mu+ mu- e+ e-')) | (mcMatch('KS0 => mu+ ^mu- e+ e-'))"
preambulo =[ "from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch" ]
_Filter = FilterDesktop(Preambulo = preambulo,Code = _code)

MatchedMuons = Selection("MatchedLongMuons",Algorithm = _Filter, RequiredSelections = [StdAllNoPIDsMuons] )
				       
MatchedMuons_seq = SelectionSequence("MatchedLongMuonsSequence", TopSelection=MatchedMuons)

    
#
## Setting up combinations
#

combs = {"LL":"( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 2 )",
         "UU":"( ANUM( ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 2 )",
         "VV":"( ANUM( ( TRTYPE == 1 ) &  ( ABSID == 'e-' ) ) == 2 )",
         "LU":"( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 1 ) & ( ANUM( ( TRTYPE == 4 ) & ( ABSID == 'e-' ) ) == 1 ) )",
         "LV":"( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 1 ) & ( ANUM( ( TRTYPE == 1 ) & ( ABSID == 'e-' ) ) == 1 ) )",
         "UV":"( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 1 ) & ( ANUM( ( TRTYPE == 1 ) & ( ABSID == 'e-' ) ) == 1 ) )"}



selsKs2mu2e={}
seqsks2mu2e={}
Ks2mu2e={}
for name in combs:
    Ks2mu2e[name] = CombineParticles("TrackSel"+name+"_Ks2mu2e")

    if "V" in name:
        Ks2mu2e[name].DecayDescriptors = ["KS0 -> mu+ mu- e+ e-","KS0 -> mu+ mu- e+ e+","KS0 -> mu+ mu- e- e-"]
	
    else:
        Ks2mu2e[name].DecayDescriptor = "KS0 -> mu+ mu- e+ e-"
    Ks2mu2e[name].Preambulo=["from LoKiPhysMC.decorators import *",
                               "from LoKiPhysMC.functions import mcMatch"]
     

    
    Ks2mu2e[name].CombinationCut = combs[name]
    Ks2mu2e[name].MotherCut = "ALL"
    


    Ks2mu2e[name].Inputs =['Phys/MatchedLongMuons/Particles']
    if "U" in name:
	    Ks2mu2e[name].Inputs+=['Phys/MatchedUpElectrons/Particles']
    if "V" in name:
	    Ks2mu2e[name].Inputs+=['Phys/MatchedVeloElectrons/Particles']
    if "L" in name:
	    Ks2mu2e[name].Inputs+=['Phys/MatchedLongElectrons/Particles']

    if name=="UV":
	    selsKs2mu2e[name]= Selection("SelKs2mu2e_"+name,
			      Algorithm = Ks2mu2e[name],
                              
			      RequiredSelections=[
						  AutomaticData(Location = 'Phys/MatchedLongMuons/Particles'),
    			       			  AutomaticData(Location ='Phys/MatchedUpElectrons/Particles'),
    			       			  AutomaticData(Location = 'Phys/MatchedVeloElectrons/Particles')])
	    
    if name == "LV":
	    selsKs2mu2e[name]= Selection("SelKs2mu2e_"+name,
			      Algorithm = Ks2mu2e[name],
                              
			      RequiredSelections= [AutomaticData(Location = 'Phys/MatchedLongElectrons/Particles'),
						  AutomaticData(Location = 'Phys/MatchedLongMuons/Particles'),
    			       			  AutomaticData(Location ='Phys/MatchedVeloElectrons/Particles')
    			       			  ])
    if name == "VV" :
	    selsKs2mu2e[name]= Selection("SelKs2mu2e_"+name,
			      Algorithm = Ks2mu2e[name],
                              
			      RequiredSelections=[
						  AutomaticData(Location = 'Phys/MatchedLongMuons/Particles'),
    			       			  AutomaticData(Location ='Phys/MatchedVeloElectrons/Particles')
    			       			  ])
    if name == "LL" : 
	    selsKs2mu2e[name]= Selection("SelKs2mu2e_"+name,
			      Algorithm = Ks2mu2e[name],
                              
			      RequiredSelections=[AutomaticData(Location = 'Phys/MatchedLongElectrons/Particles'),
						  AutomaticData(Location = 'Phys/MatchedLongMuons/Particles')
    			       			  ])
    if name == "LU":
	    selsKs2mu2e[name]= Selection("SelKs2mu2e_"+name,
			      Algorithm = Ks2mu2e[name],
                              
			      RequiredSelections=[AutomaticData(Location = 'Phys/MatchedLongElectrons/Particles'),
						  AutomaticData(Location = 'Phys/MatchedLongMuons/Particles'),
						  AutomaticData(Location = 'Phys/MatchedUpElectrons/Particles')
    			       			  ])
    if name == "UU":
	    selsKs2mu2e[name]= Selection("SelKs2mu2e_"+name,
			      Algorithm = Ks2mu2e[name],
                              
			      RequiredSelections=[
						  AutomaticData(Location = 'Phys/MatchedLongMuons/Particles'),
						  AutomaticData(Location = 'Phys/MatchedUpElectrons/Particles')
    			       			  ])
	    
    seqsks2mu2e[name]=SelectionSequence("MCFilter_"+name,TopSelection=selsKs2mu2e[name])
                       

#
## Merging streams
#

from StrippingConf.Configuration import StrippingConf
from StrippingConf.StrippingLine import StrippingLine
from StrippingConf.StrippingStream import StrippingStream

strilines = {}
for key in selsKs2mu2e:
	strilines[key] = StrippingLine("line"+key,
				       prescale  = 1,
				       postscale = 1,
				       selection = selsKs2mu2e[key])
	
	
mystream = StrippingStream(name="KS02mu2e",Lines = strilines.values())

sc = StrippingConf( Streams = [ mystream ],
                    MaxCandidates = -1,
                    TESPrefix = 'Strip'
                    )

mystream.sequence().IgnoreFilterPassed = False # so we filter events by stripping selected lines


#
## Write DST
#

enablePacking = True
dstExtension = ".dst"

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default' : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default' : stripDSTStreamConf(pack=enablePacking,
                                   selectiveRawEvent=False)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='MCMatch',
			  SelectionSequences = sc.activeStreams()
                          )




# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41452811)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number




#
## DaVinci Configuration
#

DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence([myseq])
DaVinci().appendToMainSequence([ MatchedElectrons_seq[key] for key in MatchedElectrons_seq ])
DaVinci().appendToMainSequence([ MatchedMuons_seq])
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([ stck ])
DaVinci().appendToMainSequence([ dstWriter.sequence() ])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

