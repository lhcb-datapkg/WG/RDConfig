"""
Stripping29r2p1 filtered production for streams: 'B2XGammaExclTDCPV', 'B2XGamma'
@author Biplab Dey
@date   2020-04-23
"""
'''
#------Remove---------
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#---------------------
'''
#stripping version
stripping='stripping29r2p1'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = []
streams.append( buildStream(stripping=config, streamName='Leptonic', archive=archive) )
streams.append( buildStream(stripping=config, streamName='BhadronCompleteEvent', archive=archive) )

AllStreams = StrippingStream("Beauty2XGamma.Strip")

linesToAdd = []
for stream in streams:
    for line in stream.lines:
        if 'Beauty2XGamma' in line.name(): linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip' )


AllStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,
                                                 #selectiveRawEvent=True,
                                                 fileExtension='.dst')
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
#https://gitlab.cern.ch/lhcb-datapkg/AppConfig/-/blob/master/options/DaVinci/DV-Stripping29r2p1-Stripping-MC-DST.py#L87
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x42922921)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"

#------Remove---------
#DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
#---------------------
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

'''
#------Remove---------
# comment this part out
DaVinci().DataType = "2017"
# Save info of rejection factor in the filtering
from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output2017.txt"
DaVinci().MoniSequence += [ DumpFSR() ]

from GaudiConf import IOHelper
# Use the local input data

# Bd->K*gamma
# evt+std://MC/2017/11102202/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/ALLSTREAMS.DST
IOHelper().inputFiles([
    '/eos/lhcb/grid/prod//lhcb/MC/2017/ALLSTREAMS.DST/00086280/0000/00086280_00000001_7.AllStreams.dst',
    '/eos/lhcb/grid/prod//lhcb/MC/2017/ALLSTREAMS.DST/00086280/0000/00086280_00000003_7.AllStreams.dst',
    '/eos/lhcb/grid/prod//lhcb/MC/2017/ALLSTREAMS.DST/00086280/0000/00086280_00000005_7.AllStreams.dst',
    '/eos/lhcb/grid/prod//lhcb/MC/2017/ALLSTREAMS.DST/00086280/0000/00086280_00000006_7.AllStreams.dst',
    ], clear=True)
'''

