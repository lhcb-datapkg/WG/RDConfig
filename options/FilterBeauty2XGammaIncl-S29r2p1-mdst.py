"""
Stripping29r2p1 filtered production for Beauty2XGamma inclusive lines
@author Preema Pais
@date   2021-01-21
"""

#use CommonParticlesArchive
stripping='stripping29r2p1'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

AllStreams = StrippingStream("Beauty2XGammaIncl.Strip")

linesToAdd = []
for stream in streams:
    if 'Leptonic' in stream.name():
        for line in stream.lines:
            if 'Beauty2XGamma2pi_' in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
            if 'Beauty2XGamma3pi_' in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
            if 'Beauty2XGamma4pi_' in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
            if 'Beauty2XGammapi_' in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
            if 'Beauty2XGammaphiOmega_' in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip' )

AllStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements)

SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,
                                                    isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking,
                                                      selectiveRawEvent=False,
                                                      isMC=True)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x42922921) 
#0xVVVVVSSS, VVVVV DaVinci version, SSS Stripping number

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

#######################################################################
## comment this part out
#DaVinci().DataType = "2017"
## Save info of rejection factor in the filtering
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]

#from GaudiConf import IOHelper
## Use the local input data
#IOHelper().inputFiles([
#'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00086661/0000/00086661_00000105_7.AllStreams.dst']
#, clear=True)



