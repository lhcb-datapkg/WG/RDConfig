"""
Stripping Filtering file for the Bu2LLK lines. Prescales removed. WRITES AN MDST
@author Alexander Shires
@date   2013-07-22

"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"




# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }






#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20r0p2'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

_leptonic         = quickBuild('Leptonic')
_leptonic.lines[:] = [ x for x in _leptonic.lines if 'Bu2LLK' in x.name() ]
for line in _leptonic.lines :
   print "leptonic has a line called " + line.name()

streams.append( _leptonic )

# turn off all pre-scalings 
for stream in streams: 
    for line in stream.lines:
        line._prescale = 1.0 

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

#
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("Bu2LLK.Strip")

for stream in streams:
    AllStreams.appendLines(stream.lines)

sc = StrippingConf( Streams = [ AllStreams ],
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents, 
                    MaxCandidates = 2000 )

AllStreams.sequence().IgnoreFilterPassed = False # filtering mode

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      #stripMicroDSTStreamConf,
                                      #stripMicroDSTElements,
                                      #stripCalibMicroDSTStreamConf
                                      )

#
# Configuration of MicroDST
#
#mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking)
#mdstElements   = stripMicroDSTElements(pack=enablePacking)
#leptonicMicroDSTname   = 'Leptonic'

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    #leptonicMicroDSTname    : mdstElements
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf(pack=enablePacking)
    #leptonicMicroDSTname     : mdstStreamConf
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2012"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
#DaVinci().UseTrigRawEvent=True
# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
