########################################################################
'''
   Filtering for various rare strange decays (and normalization channels)
   Only reconstruction filtering, no stripping
   @author: Francesco Dettori
   @date: 28/10/2014

'''
from Gaudi.Configuration import *
from Configurables import DaVinci, CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPions, StandardMCMuons, StandardMCPi0, StandardMCProtons, StandardMCElectrons

#Truth matched commonparticles:
stdparticles = {
    "pi+" : DataOnDemand(Location='Phys/StdMCPions/Particles'),
    "pi0" : DataOnDemand(Location='Phys/StdMCPi0/Particles'),
    "p+"  : DataOnDemand(Location='Phys/StdMCProtons/Particles'),
    "mu+" : DataOnDemand(Location='Phys/StdMCMuons/Particles'),
    "e+" : DataOnDemand(Location='Phys/StdMCElectrons/Particles')
    }

# Decays to be matched 
decays = { 'sigmapee': '[Sigma+ ==> p+ e- e+]CC' }
           
#
# MC matching
#
matchers = { dec : "(mcMatch('" + decays[dec] +  "'))" for dec in decays }

#
# Selection sequences
#
selection_sequences = [ ]
for dec in decays :
    input_particles = []
    for std in stdparticles :
        if ( std in decays[dec] ) or ( std.replace('+','-') in decays[dec] ):
            input_particles.append( stdparticles[ std ] )
    decaydescriptor = decays[ dec ].replace("==>","->").replace("CC","cc")
    cmb = CombineParticles(dec)
    cmb.DecayDescriptor = decays[ dec ].replace("==>","->").replace("CC","cc")   #modified decay descriptor for CombParticles
    cmb.MotherCut = matchers[ dec ]
    cmb.Preambulo = [
        "from LoKiPhysMC.decorators import *",
        "from PartProp.Nodes import CC" ]
        
    sel = Selection( "Sel"+dec
                     , Algorithm = cmb
                     , RequiredSelections =  input_particles  )
    Seq = SelectionSequence('MCFilter'+dec, TopSelection = sel ) 
    selection_sequences.append( Seq )

#
# Write DST
#
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = selection_sequences,
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                       # Number of events

DaVinci().appendToMainSequence( [ s.sequence() for s in selection_sequences ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

## DMS: run with Francesco's options:
DaVinci().DataType = "2012" #dms
from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
DaVinci().MoniSequence += [ DumpFSR() ]


