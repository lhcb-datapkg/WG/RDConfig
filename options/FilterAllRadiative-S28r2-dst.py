"""
Stripping28r2 filtered production for all radiative streams. Selects any stripping
lines whose name includes either "Beauty2X" or "Lb", in addition to "Gamma". 

Outputs to DST.

Retention rate of 8.6% when tested on Bd2KstGamma (11102202) events.

Adapted from FilterBeauty2XGamma-S28r2-dst.py.

@author Izaac Sanderswood
@date   2021-11-05
"""
'''
#------Remove---------
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#---------------------
'''

#stripping version
stripping='stripping28r2'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

from StrippingSelections.Utils import lineBuilder, buildStreams
streams = buildStreams( config, WGs='RD' )

AllStreams = StrippingStream("AllRadiative.Strip")

linesToAdd = []
for stream in streams:
    for line in stream.lines:
        if ( 'Beauty2X' in line.name() or 'Lb' in line.name() ) and 'Gamma' in line.name():
        		line._prescale = 1.0
        		linesToAdd.append(line) 
AllStreams.appendLines(linesToAdd)
sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip' )


AllStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,
                                                 #selectiveRawEvent=True,
                                                 fileExtension='.dst')
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44105282)
# https://gitlab.cern.ch/lhcb-datapkg/AppConfig/-/blob/master/options/DaVinci/DV-Stripping28r2-Stripping-MC-DST.py#L87

# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"]
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"

#------Remove---------
#DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
#---------------------
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

'''
#------Remove---------
### comment out
## tested with `lb-run DaVinci/v44r10p5 gaudirun.py FilterAllRadiative-S28r2-dst.py`
DaVinci().DataType = "2016"
# Save info of rejection factor in the filtering
from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output2016.txt"
DaVinci().MoniSequence += [ DumpFSR() ]

from GaudiConf import IOHelper
# Use the local input data

# Bd->K*gamma
#evt+std://MC/2016/11102202/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/DST
IOHelper().inputFiles([
    '/eos/lhcb/grid/prod//lhcb/MC/2016/DST/00058298/0000/00058298_00000024_1.dst',
#    '/eos/lhcb/grid/prod//lhcb/MC/2016/DST/00058298/0000/00058298_00000005_1.dst',
#    '/eos/lhcb/grid/prod//lhcb/MC/2016/DST/00058298/0000/00058298_00000055_1.dst',
#    '/eos/lhcb/grid/prod//lhcb/MC/2016/DST/00058298/0000/00058298_00000048_1.dst'
    ], clear=True)
'''

