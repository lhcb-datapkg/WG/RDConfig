"""
Stripping Filtering file for Sigma -> p pi0 S28r2 line
@author Francesca Dordei
@date   2022-01-27
"""


from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

stripping = 'stripping28r2'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.3)


#get the line builders from the archive
archive = strippingArchive(stripping)
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
streams = buildStreams(stripping = config, archive = archive)

lines = []
for stream in streams:
    if 'Leptonic' in stream.name():
        for line in stream.lines:
            if 'SigmaPPi0' in line.name():
                line._prescale = 1.0
                lines.append(line)

AllStreams = StrippingStream("SigmaPPi0.Strip")
AllStreams.appendLines(lines)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    #HDRLocation = 'TempStrip', # TO BE REMOVED FOR MC PRODUCTION
                    TESPrefix = 'Strip'
                    )

# So that we get only selected events written out
AllStreams.sequence().IgnoreFilterPassed = False

enablePacking = True
#
# Configuration of SelDSTWriter
#
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTElements,
                                      stripDSTStreamConf
                                      )

SelDSTWriterElements = {
    'default' : stripDSTElements(pack=enablePacking)
    }


SelDSTWriterConf = {
    'default' : stripDSTStreamConf(pack=enablePacking)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='SigmaPPi0',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44105282)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"]
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = '2016'
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = 'DVHistos.root'
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

#######################################################################
# comment this part out
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]
