"""
Options for building Stripping17 for DiMuon stream filtering.
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping17'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

streams.append( quickBuild('Dimuon') )

#
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("DiMuon")

for stream in streams:
    AllStreams.appendLines(stream.lines)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000 )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get all events written out

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                       # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
