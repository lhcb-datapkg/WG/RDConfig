"""
Stripping Filtering file for Bu2LLK+B2LLXBDT+B2XMuMu S34r0p2 (2018) line
@author Biplab Dey
@date   2021-12-26
"""

#stripping version
stripping='stripping34r0p2'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)


#
# Merge into one stream and run in filter mode
#
AllStreams = StrippingStream("Bu2LLK_B2LLXBDT_B2XMuMu.Strip")

linesToAdd = []
for stream in streams:
    if 'Leptonic' in stream.name():
        for line in stream.lines:
            if 'Bu2LLK' in line.name() or 'B2LLXBDT' in line.name() or 'B2XMuMu' in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so we filter events by stripping selected lines

#
# Configure the dst writers for the output
#
enablePacking = True
#enablePacking = False

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripMicroDSTStreamConf,
                                       stripMicroDSTElements
                                       )

#
# Configuration of MicroDST
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
mdstElements   = stripMicroDSTElements(pack=enablePacking, isMC=True)
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking,
                                         selectiveRawEvent=True,
                                         isMC=True)

#
# Configuration of SelDSTWriter
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
SelDSTWriterElements = {
    'default'               : mdstElements,
    }

SelDSTWriterConf = {
    'default'                : mdstStreamConf,
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44113402)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
#DaVinci().DataType = "2018"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

"""
# Save info of rejection factor in the filtering
from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
DaVinci().MoniSequence += [ DumpFSR() ]

import glob
pkmm_2011  = glob.glob("/eos/lhcb/grid/prod/lhcb/MC/2011/ALLSTREAMS.DST/00139086/0000/00139086_0*_5.AllStreams.dst")
kstee_2011 = glob.glob("/eos/lhcb/grid/prod//lhcb/MC/2011/ALLSTREAMS.DST/00111873/0000/00111873_0000*dst")
pkmm_2012  = glob.glob("/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00139094/0000/00139094_000*")
kstee_2012  = glob.glob("/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00053701/0000/00053701_00000*")
l1520mm_2016  = glob.glob("/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00109841/0000/00109841_000*")
kstee_2016  = glob.glob("/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00057750/0000/00057750_000*")
pkmm_2017 = glob.glob("/eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00108536/0000/001*")
pkee_2017 = glob.glob("/eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00108534/0000/00108534_000000*")
pkmm_2018 = glob.glob("/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00107941/0000/00107941_000*")
pkee_2018 = glob.glob("/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00107947/0000/00107947_0000*")

from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(pkee_2018[:10], clear=True)
"""


