"""
Stripping Filtering file for the Tau2PhiMu line with also a looser line defined 
@author Giulio Dujany
@date   2014-14-04

PAY ATTENTION: If your line is prescaled in the stripping you might want to adjust that!
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"


#
# Configuration of My costumized stripping line
#

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData
from StandardParticles import StdLooseMuons, StdLooseKaons
from Configurables import FilterDesktop, CombineParticles, OfflineVertexFitter, LoKi__HDRFilter 
from GaudiKernel.PhysicalConstants import c_light


def makeMySelection(name=''):
    '''
    Arguments:
    name: name of the Selection
    '''
    # Muons and kaons
    looseMuons = DataOnDemand(Location = 'Phys/StdLooseMuons/Particles')
    looseKaons = DataOnDemand(Location = 'Phys/StdLooseKaons/Particles')


    # Select Phi
    recoPhi = CombineParticles(name+"Phi2KK")
    recoPhi.DecayDescriptor = 'phi(1020) -> K+ K-'
    recoPhi.DaughtersCuts = {"K+": "(ISLONG) & (TRCHI2DOF < 3 ) & (TRGHOSTPROB<0.3) & ( BPVIPCHI2 () >  9 ) & (PT>300*MeV) "}#& (PIDK > 5)"} # No PID cut
    recoPhi.CombinationCut =  "(ADAMASS('phi(1020)')<30*MeV)" # "(ADAMASS('phi(1020)')<30*MeV instead of 10)" 
    recoPhi.MotherCut = " ( VFASPF(VCHI2) < 10 ) & (MIPCHI2DV(PRIMARY)> 16.)"


    phi_selection = Selection(name+'SelPhi2KK', Algorithm = recoPhi, RequiredSelections = [ looseKaons ])


    # Select Tau
    recoTau = CombineParticles(name+"Tau2PhiMu")
    recoTau.DecayDescriptor = '[tau- -> phi(1020) mu-]cc'
    recoTau.DaughtersCuts = { "mu-" : " ( PT > 300 * MeV )  & ( BPVIPCHI2 () >  9 ) & ( TRCHI2DOF < 3 )& (TRGHOSTPROB<0.3)" }
    recoTau.CombinationCut = "(ADAMASS('tau-')<150*MeV)"

    recoTau.MotherCut = "( VFASPF(VCHI2) < 10 ) &  ( (BPVLTIME () * c_light)   > 200 * micrometer ) &  ( BPVIPCHI2() < 100 ) "

    tau_selection = Selection(name+"SelTau2PhiMu", Algorithm = recoTau, RequiredSelections = [ looseMuons, phi_selection ])

    return tau_selection



from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

class MyLineBuilderConf(LineBuilder):

    __configuration_keys__ = ( 'Postscale', 'Prescale')

    config_default={
        'Postscale'  :1,
        'Prescale'   :1,
        }

    def __init__(self , name='' , config=None) :
        LineBuilder.__init__(self , name , config=config)
        self.tau_selection = makeMySelection(name)
        self.makeLine(name , config)

    

    def makeLine(self, name , config) :
        line = StrippingLine(name+'Tau2PhiMu'+'Line',
                             prescale=1.,
                             selection=self.tau_selection)
        self.registerLine( line )


config = { 'Postscale'  :1, 'Prescale'   :1}

myBuilder = MyLineBuilderConf('MyLoose', config, )


#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

_dimuon         = quickBuild('Dimuon')
_dimuon.lines[:] = [ x for x in _dimuon.lines if 'Tau2PhiMu' in x.name() ]
for line in _dimuon.lines :
   print "dimuon has a line called " + line.name()
streams.append( _dimuon )

AllStreams = StrippingStream("Tau2PhiMu.Strip")

for stream in streams:
    AllStreams.appendLines(stream.lines)

## Add also my line
AllStreams.appendLines( myBuilder.lines() )
###

# Have to change the location, can't be Strip because there already are the DecReports when I test on tagged MC
# The location of DecReports then will be "/Event/Filter/Phys/DecReports"
prefix = 'Strip' #'Filter'

sc = StrippingConf( Streams = [ AllStreams ],
                    TESPrefix = prefix,
                    HDRLocation='Phys/DecReports',
                    MaxCandidates = 2000 )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements


#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements(stripPrefix=prefix)
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf(stripPrefix=prefix)
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='RD',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK                                                                                                                         
# from Configurables import StrippingTCK
# stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x32210200)

# Fancy things
from Configurables import StrippingReport, AlgorithmCorrelationsAlg
report = StrippingReport(Selections = sc.selections())
correlations = AlgorithmCorrelationsAlg(Algorithms = sc.selections())


#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2012"
DaVinci().Simulation = True
DaVinci().EvtMax = 1000#-1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ report, correlations ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
#DaVinci().UseTrigRawEvent=True
