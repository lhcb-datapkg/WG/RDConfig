######################################################################
# 3) Filters
#
#
#
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    L0DU_Code  = " L0_CHANNEL('Muon') "
    , HLT_Code   = " HLT_PASS_RE('Hlt1.*Track.*Decision') & ( HLT_PASS_RE('Hlt2.*Topo.*Decision') | HLT_PASS_RE('Hlt2SingleMuonDecision') | HLT_PASS_RE('Hlt2DiMuonDetachedDecision') )" 
    , STRIP_Code = " HLT_PASS('StrippingBd2KstarMuMu_BdToKstarMuMuLineDecision') | HLT_PASS('StrippingBd2KstarMuMu_BdToKstarMuMuLowPLineDecision') "
    )


from Configurables import DaVinci
DaVinci().EventPreFilters = trigfltrs.filters('KstmmFilters')


